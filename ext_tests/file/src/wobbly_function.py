"""
Usage:
    python3 function.py -filebasename <folder-to-store-output>/<somefilebasename> -x 3 -y 5.5
"""

from argparse import ArgumentParser
import csv
import numpy as np
import time
import yaml

def function(x, y):
    '''Returns wobbly function value'''

    radius_squared = (x) ** 2 + (y) ** 2
    return radius_squared + np.sin(radius_squared)


if __name__ == "__main__":
    # Parse the input argument
    parser = ArgumentParser()
    parser.add_argument('--filebasename', type=str)
    parser.add_argument('--configfile', type=str)
    parser.add_argument('--paramfile', type=str)
    #parser.add_argument('--x', type=float)
    #parser.add_argument('--y', type=float)
    args = parser.parse_args()
    with open(args.paramfile, 'r') as f:
        parameters = yaml.load(f, Loader=yaml.FullLoader)

    # Run the "simulation"
    output_value = function(x=parameters['x'], y=parameters['y'])

    # Store the output in a file
    csv_filename = args.filebasename + ".csv"
    with open(csv_filename, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow([output_value] + list(parameters.values()))
