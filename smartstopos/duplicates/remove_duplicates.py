#! /usr/bin/env python3
"""
Removes parameter combinations that have already been computed and appends the results into the current generation

Usage:
    python remove_duplicates.py --step
"""
from argparse import ArgumentParser
import re


def read_file(file_name_or_buffer, include_original_line=False):
    """reads a csv like file and yields the values parsed as floats together with the line string if requested"""
    with open(file_name_or_buffer, 'r') as f:
        for line in f:
            line = re.sub(r'\s*\t\s*', ',', line.strip())
            vals = [float(value.strip()) for value in line.split(',')]
            yield (vals, line) if include_original_line else vals


def find_duplicate_lines(new_parameters_file, old_parameters_file):
    """finds the lines in the first file that also exist in the second file"""
    old_parameters = list(read_file(old_parameters_file))

    return {
        line_index: new_params
        for line_index, new_params in enumerate(read_file(new_parameters_file))
        if new_params in old_parameters  # TODO: compare better! maybe np.isclose?
    }


def remove_duplicates(file_name_or_buffer, duplicate_lines):
    """saves the file containing duplicated lines without them"""
    unique_lines = [
        line.replace(',', '\t')
        for index, (vals, line) in enumerate(read_file(file_name_or_buffer, include_original_line=True))
        if index not in duplicate_lines
    ]

    with open(file_name_or_buffer, 'w') as f:
        f.write('\n'.join(unique_lines))


def write_duplicates_file(prev_output_file, next_duplicates_file, duplicate_lines):
    """writes a file with all duplicate lines together with the cost calculated in previous runs"""
    duplicate_params = list(duplicate_lines.values())

    duplicate_lines_with_cost = [
        line
        for cost_params, line in read_file(prev_output_file, include_original_line=True)
        if cost_params[1:] in duplicate_params
    ]

    with open(next_duplicates_file, 'w') as f:
        f.write('\n'.join(duplicate_lines_with_cost))


def main(step):
    """finds duplicate parameters between current and previous step, removes them in current file and write the
    duplicates file with the cost for those parameters"""
    opt_step = step
    next_step = step + 1

    old_file_set = "param_set_" + str(opt_step)
    new_file_set = "param_set_" + str(next_step)

    duplicate_lines = find_duplicate_lines(new_file_set, old_file_set)
    remove_duplicates(new_file_set, duplicate_lines)
    write_duplicates_file('opt_step_{}/csv_output.csv'.format(opt_step), 'duplicates.csv', duplicate_lines)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--step', required=True, type=int, help='current opt step')
    args = parser.parse_args()

    main(args.step)
