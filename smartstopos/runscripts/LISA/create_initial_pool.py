#!/usr/bin/env python3
"""
Creates a param_set files to be added to the stopos pool based on the parameters
input file.

Usage:
    python create_initial_pool.py --input_file input_file.ini
"""

from argparse import ArgumentParser
import random
import logging
from smartstopos.utils.stopostools import create_stopos_file
from smartstopos.utils.readinput import read_input
from smartstopos.utils.makedatapoints import make_data_points


def main():
    """ Function to create an initial pool of parameters based on input file."""
    parser = ArgumentParser()
    parser.add_argument('--input_file', required=False, default="input_file.ini", type=str,
                        help='File with the input information about the simulation and parameters')
    args = parser.parse_args()

    logging.basicConfig(filename='sim.log', level=logging.DEBUG)
    # initialize default sim_param:
    sim_param = {'number_best_candidates': 10, 'proba_mutation': 1.0,
                 'proba_crossover': 1.0, 'global_scale_factor': 1.0, 'maximum':
                 False, 'seed': random.random(), 'run_type': 'single',
                 'algorithm': None}

    # read the parameters input file and create set of parameters to explore
    set_param = read_input(args.input_file, sim_param)
    make_data_points(set_param)
    # create initial param_set file to be used by stopos
    opt_step = 0
    print("Current optimization step: {}".format(opt_step))
    print("Simulation details:")
    for item in sim_param.items():
        print(item)
    print("Parameters to explore:")
    set_param.list_parameters()
    create_stopos_file(set_param, number_runs=1, name=str(opt_step))


if __name__ == "__main__":
    main()
