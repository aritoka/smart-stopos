#!/bin/bash

# read general info from input file needed to set up simulations 
echo "Reading information from input file"
opt_steps=0
input_file=input_file.ini #(default name)
run_program=$(sed -n '/run_program/p' $input_file | awk '{print $2}')
venvdir=$(sed -n '/venvdir/p' $input_file | awk '{print $2}')
sstoposdir=$(sed -n '/sstoposdir/p' $input_file | awk '{print $2}')
workdir=$(sed -n '/workdir/p' $input_file | awk '{print $2}')
workdir=$(sed -n '/workdir/p' $input_file | awk '{print $2}')

number_args=$(sed -n '/run_program/p' $input_file | awk '{print $3}')
for j in $(seq 1 $number_args)
do
	variable=$[$j+3]
	eval "arg$j=$(sed -n '/run_program/p' $input_file | awk '{print $'$variable'}')"
done
analysis_program=$(sed -n '/analysis_program/p' $input_file | awk '{print $2}')
files=$(sed -n '/files/p' $input_file | awk '{for (i=2; i<=NF; i++) print $i}')
name=$(sed -n '/name/p' $input_file| awk '{for (i=2; i<=NF; i++) print $i}')
description=$(sed -n '/description/p' $input_file| awk '{for (i=2; i<=NF; i++) print $i}')
run_type=$(sed -n '/run_type/p' $input_file| awk '{print $2}')
if [[ "$run_type" == "optimization" ]]; then
    algorithm=$(sed -n '/run_type/p' $input_file| awk '{print $3}')
    opt_steps=$(sed -n '/run_type/p' $input_file| awk '{print $4}')
fi

echo "name run : $name"
echo "run program : $run_program"
echo "files needed: $files"
echo "workdir: $workdir"
echo "sstoposdir: $sstoposdir"
echo "venv: $venvdir"
echo "analysis_program : $analysis_program"
echo "run type: $run_type"

export PYTHONPATH=$PYTHONPATH:$sstoposdir
export PATH=$PATH:$sstoposdir/smartstopos/pools

## create directory structure 
echo "Creating directory structure"
cd $workdir
sourcedir=$workdir/source
outputdir=$workdir/output
time_stamp=$(date +'%Y-%m-%d_%H.%M')
#folder_name=$name\_$time_stamp
folder_name=$name

mkdir -p $outputdir/$folder_name
if [[ "$run_type" == "optimization" ]]; then
    run_dir=$outputdir/$folder_name/optimization
    mkdir -p $run_dir
fi
if [[ "$run_type" == "single" ]]; then
    run_dir=$outputdir/$folder_name/single_step
    mkdir -p $run_dir
fi
# make sure you add any other files needed by your simulations
echo "Files copied to running directory: $run_dir"
cp $sourcedir/input_file.ini $sourcedir/$files  $run_dir
cd $run_dir


#create file with parameters to run 
create_initial_pool.py  --input_file $input_file 
echo "Initial set of parameter created: param_set_0"

#Running simulations
count=0
echo "Starting simulations"
# SINGLE RUN 
if [[ "$run_type" == "single" ]]; then
    while read LINE
        do
		count=$[$count+1]
		for j in $(seq 1 $number_args)
		do 
			name=$(echo arg"$j")
			value=$(echo $LINE |cut -d\  -f"$j")
			variables="${variables}$(echo --${!name} $value " ")" 
			if [[ "$j" == "$number_args" ]]; then
				filename="${filename}$(echo $value)"
			else
				filename="${filename}$(echo $value\_)"
			fi
		done
        #filebasename="test"_$count     #if many variables, use a counter
        filebasename="test"_$filename 
		echo $count running $LINE
        python3 $run_program  --filebasename $filebasename $variables
		unset variables
		unset filename
        wait
    done < param_set\_0
	echo "Starting analysis"
	mkdir output
	mv test* output
    python3 $analysis_program
fi
# end SINGLE RUN

# OPTIMIZATION LOOP
if [[ "$run_type" == "optimization" ]]; then
    # loop over optimization steps
    for i in $(seq 0 $opt_steps)
        do
		echo "Starting simulations opt_step $i"
        # copy files to opt_step
        mkdir -p opt_step\_$i
        cp $files input_file.ini param_set\_$i opt_step\_$i 
        opt_name=$run_dir/opt_step\_$i
        cd $opt_name
    
    	while read LINE
    	    do
			count=$[$count+1]
			for j in $(seq 1 $number_args)
			do 
				name=$(echo arg"$j")
				value=$(echo $LINE |cut -d\  -f"$j")
				variables="${variables}$(echo --${!name} $value " ")" 
				if [[ "$j" == "$number_args" ]] 
				then
					filename="${filename}$(echo $value)"
			    fi	
				if [[ "$j" < "$number_args" ]] 
				then
					filename="${filename}$(echo $value\_)"
				fi
			done
    	    filebasename="test"_$filename
    	    #filebasename="test"_$count 
    	    python3 $run_program  --filebasename $filebasename $variables
			unset variable
			unset filename
    	    wait
    	done < param_set\_$i
		echo "Finished running parameters opt_step_$i"
		echo "Starting analysis opt_step $i"
		mkdir output
		mv test_* output
        python3 $analysis_program 
        cd $run_dir
		echo "Creating new optimized parameter from results opt_step_$i"
        create_opt_pool.py  --opt_step $i --prev_directory $opt_name > out_opt\_$i
		echo "New parameters set created"
		echo ""
    done
fi
# end OPTIMIZATION LOOP
