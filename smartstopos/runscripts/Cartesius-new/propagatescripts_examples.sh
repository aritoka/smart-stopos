#!/bin/bash

for file in "$@"
do 
    cp $file  ../../../examples/ae_David/src/
    cp $file  ../../../examples/demo_wobbly_function_single_parameter/src/
    cp $file  ../../../examples/demo_wobbly_function/src/
    cp $file  ../../../examples/quartic_function/src/
    cp $file  ../../../examples/demo_wobbly_function_constrain+/src/
    cp $file  ../../../examples/rastrigin_function/src/
    cp $file  ../../../examples/demo_wobbly_function_gaussians/src/
    
    git add ../../../examples/ae_David/src/$file
    git add ../../../examples/demo_wobbly_function_single_parameter/src/$file
    git add ../../../examples/demo_wobbly_function/src/$file
    git add ../../../examples/quartic_function/src/$file
    git add ../../../examples/demo_wobbly_function_constrain+/src/$file
    git add ../../../examples/rastrigin_function/src/$file
    git add ../../../examples/demo_wobbly_function_gaussians/src/$file
    git add $file 
done


git commit -m "Update runscripts example"

       

