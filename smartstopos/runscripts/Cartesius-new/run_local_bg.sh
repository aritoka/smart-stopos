#!/bin/bash
#SBATCH --nodes=1
#SBATCH --time=00:05:00
#SBATCH --partition=normal

currentdir=$PWD
workdir=$(echo $currentdir | sed 's/\/src//')
echo $workdir
tmp_dir="$(mktemp -d -p /scratch-shared)"
echo $tmp_dir
cd $workdir
cp -r src/ $tmp_dir
cd $tmp_dir/src
./run_local.sh
cp -r $tmp_dir/output $workdir
rm -r $tmp_dir

