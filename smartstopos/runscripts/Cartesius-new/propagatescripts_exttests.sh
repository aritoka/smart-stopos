#!/bin/bash

for file in "$@"
do 
    cp $file  ../../../examples/ae_David/src/
    cp $file  ../../../ext_tests/opt/src/       
    cp $file  ../../../ext_tests/file/src/       
    cp $file  ../../../ext_tests/single/src/
    git add ../../../ext_tests/opt/src/$file
    git add ../../../ext_tests/file/src/$file  
    git add ../../../ext_tests/single/src/$file
done

git commit -m "Update runscripts ext_tests"
