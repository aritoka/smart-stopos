#!/bin/bash
#SBATCH --nodes=1
#SBATCH --time=100:00:00
#SBATCH --partition=normal

currentdir=$PWD
workdir=$(echo $currentdir | sed 's/\/src//')
echo $workdir
tmp_dir="$(mktemp -d -p /nfs/scratch)"
echo $tmp_dir
cd $workdir
cp -r src/ $tmp_dir
cd $tmp_dir/src
bash run_local.sh
cp -r $tmp_dir/output $workdir
rm -r $tmp_dir
