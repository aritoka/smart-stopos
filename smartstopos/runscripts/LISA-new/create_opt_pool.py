#!/usr/bin/env python3

"""
Creates a new file with the parameter values to be explored based on the output
of the previous optimization step and an optimization algorithm.
Usage:
    python create_opt_pool.py --opt_step n --prev_directory directory_previous_optstep
"""
import os
import argparse

import pandas as pd
import random
import logging

from smartstopos.algorithms.ga import genetic_algorithm
from smartstopos.algorithms.gaussians import gaussians
from smartstopos.utils.stopostools import create_opt_stopos_file
from smartstopos.utils.readinput import read_input


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--opt_step', required=True, type=int, help='Current optimization step')
    parser.add_argument('--prev_directory', required=True, type=str, help="""Directory of the output file with the
        collected results of the previous optimization step""")
    parser.add_argument('--input_file', required=False, default='input_file.ini', type=str,
                        help='Name of input file with simulation and parameters information')
    parser.add_argument('--output_file', required=False, default='csv_output.csv', type=str,
                        help='Name of output file with the collected results of the previous optimization step')
    args = parser.parse_args()

    # Read results of previous optimization step, --> be aware of the field separator
    data_file = os.path.join(args.prev_directory, args.output_file)
    data = list(pd.read_csv(data_file, sep=',', engine='python', dtype=float).values)
    logging.basicConfig(filename='sim.log', level=logging.DEBUG)
    logging.debug("Data read from input file")

    # initialize default sim_param:
    sim_param = {'number_best_candidates': 10, 'proba_mutation': 1.0,
                 'proba_crossover': 1.0, 'global_scale_factor': 1.0,
                 'maximum': False, 'seed': random.random(), 'algorithm': None, 'distribution': None}

    # Initialize parameters and overwrite simulation details from input file
    set_param = read_input(args.input_file, sim_param)
    if 'run_type' not in sim_param.keys():
        raise ValueError('Please specify a run type: single or optimization')
    if 'algorithm' not in sim_param.keys() and sim_param['run_type'] == 'optimization':
        raise ValueError('Please specify an algorithm for the optimization')
    if 'opt_steps' not in sim_param.keys() and sim_param['run_type'] == 'optimization':
        raise ValueError('Please specify a number of optimization steps orconvergence criteria')

    current_generation = args.opt_step
    print("Current optimization step: {}".format(current_generation))
    print("Simulation details:")
    for item in sim_param.items():
        print(item)
    print("Parameters to explore:")
    set_param.list_parameters()

    # Based on the choice of optimization algorithm return a new set of points to explore
    if sim_param['algorithm'] == 'GA':
        logging.debug("Starting genetic algorithms routine")
        new_data_points = genetic_algorithm(data, set_param, sim_param,
                                            args.opt_step)
    elif sim_param['algorithm'] == 'Gaussians':
        logging.debug("Starting gaussian routine")
        new_data_points = gaussians(data, set_param, sim_param, args.opt_step)
    else:
        raise ValueError("Algorithm is not defined")

    # Update data points to explore of the set of parameters
    for index, (_key, param) in enumerate(set_param.parameters.items()):
        del param.data_points[:]
        for inx, value in enumerate(new_data_points):
            param.data_points.append(value[index])
        param.number_points = len(param.data_points)
    print("Creating new parameters file")
    # Create a new param_set_optstep file to be used by stopos
    create_opt_stopos_file(set_param, name=str(args.opt_step+1))


if __name__ == "__main__":
    main()
