#! /usr/bin/env python3
"""
Removes parameter combinations that have already been computed and appends the results into the current generation

Usage:
    python remove_duplicates.py --step
"""

from copy import deepcopy
from argparse import ArgumentParser
# from collections import Counter
# from itertools import zip_longest
import re
# import shutil
import os


def main():
    """ Function to eliminate duplicates and copy necessary data"""
    parser = ArgumentParser()
    parser.add_argument('--step', required=True, type=int, help='current opt step')
    args = parser.parse_args()

    opt_step = args.step
    next_step = args.step + 1

    name1 = "param_set_" + str(opt_step)
    name2 = "param_set_" + str(next_step)
    file1 = open(name1, 'r')
    file2 = open(name2, 'r')
    data1 = []
    data2 = []
    delimiter = "\t"
    for line in file1:
        line = line.strip()
        data1.append(re.sub(r'\s*{}\s*'.format(delimiter), ',', line))
    for line in file2:
        line = line.strip()
        data2.append(re.sub(r'\s*{}\s*'.format(delimiter), ',', line))
    data1 = [list(map(float, v.split(","))) for v in data1]
    data2 = [list(map(float, v.split(","))) for v in data2]
    file1.close()
    file2.close()

    duplicates = []
    copyd2 = deepcopy(data2)
    for i in copyd2:
        if i in data1:
            duplicates.append(i)
            data2.remove(i)

    output_prev = "opt_step_" + str(opt_step)
    name3 = output_prev+"/csv_output.csv"
    file3 = open(name3, 'r')
    delimiter = ","
    data3 = [re.sub(r'\s*{}\s*'.format(delimiter), delimiter, line).rstrip() for line in file3]
    file3.close()
    data3 = [list(map(float, v.split(","))) for v in data3]

    results = []
    for v in duplicates:
        for u in data3:
            if v == u[1:]:
                results.append(u)
                break
    # intersection = [ u for v,u in zip_longest(duplicates, data3) if v[0] == u[1] and v[1] == u[2] ]
    os.remove(name2)
    file2 = open(name2, 'w')
    for item in data2:
        converted_list = [str(element) for element in item]
        joined_string = "\t".join(converted_list)
        file2.write("{} \n".format(joined_string))
    file2.close()

    file3 = open("duplicates.csv", 'w')
    for item in results:
        converted_list = [str(element) for element in item]
        joined_string = ",".join(converted_list)
        file3.write("{} \n".format(joined_string))
    file3.close()


if __name__ == "__main__":
    main()
