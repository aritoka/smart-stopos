#!/bin/bash

# load modules
module load 2019
module load Stopos/0.93-GCC-7.3.0-2.30

# read general info from input file 
echo "Reading information from input file"
opt_steps=0
input_file=input_file.ini #(default name)
run_program=$(sed -n '/run_program/p' $input_file | awk '{print $2}')
venvdir=$(sed -n '/venvdir/p' $input_file | awk '{print $2}')
sstoposdir=$(sed -n '/sstoposdir/p' $input_file | awk '{print $2}')
workdir=$(sed -n '/workdir/p' $input_file | awk '{print $2}')
nodes=$(sed -n '/nodes/p' $input_file | awk '{print $2}')
timerun=$(sed -n '/time_run/p' $input_file | awk '{print $2}')
timeanal=$(sed -n '/time_analysis/p' $input_file | awk '{print $2}')
queue=$(sed -n '/queue/p' $input_file | awk '{print $2}')
fix_variables=$(sed -n '/fix_variables/p' $input_file | awk '{$1=""; print $0}')
sed -i "s/time=.*/time=$timerun/" stopos.job
sed -i "s/partition=.*/partition=$queue/" stopos.job
sed -i "s/time=.*/time=$timeanal/" analysis.job

number_args=$(sed -n '/run_program/p' $input_file | awk '{print $3}')
for j in $(seq 1 $number_args)
do
        variable=$[$j+3]
        eval "arg$j=$(sed -n '/run_program/p' $input_file | awk '{print $'$variable'}')"
done
analysis_program=$(sed -n '/analysis_program/p' $input_file | awk '{print $2}')
files=$(sed -n '/files/p' $input_file | awk '{for (i=2; i<=NF; i++) print $i}')
name=$(sed -n '/name/p' $input_file| awk '{for (i=2; i<=NF; i++) print $i}')
description=$(sed -n '/description/p' $input_file| awk '{for (i=2; i<=NF; i++) print $i}')
run_type=$(sed -n '/run_type/p' $input_file| awk '{print $2}')
if [[ "$run_type" == "optimization" ]]; then
    algorithm=$(sed -n '/run_type/p' $input_file| awk '{print $3}')
    opt_steps=$(sed -n '/run_type/p' $input_file| awk '{print $4}')
fi

echo "name run : $name"
echo "run program : $run_program"
echo "fix variables : $fix_variables"
echo "files needed: $files"
echo "workdir: $workdir"
echo "sstoposdir: $sstoposdir"
echo "venv: $venvdir"
echo "analysis_program : $analysis_program"
echo "run type: $run_type"

export PYTHONPATH=$PYTHONPATH:$sstoposdir
export PATH=$PATH:$sstoposdir/smartstopos/pools
if [[ $venvdir != "NONE" ]]; then
		source $venvdir/activate
fi


# create directory structure 
echo "Creating directory structure"
sourcedir=$workdir/src 
outputdir=$workdir/output
mkdir $outputdir
cd $sourcedir

time_stamp=$(date +'%Y-%m-%d_%H.%M')
folder_name=$name\_$time_stamp
#folder_name=$name
tmp_dir="$(mktemp -d -p /scratch-shared)"
tmp_rundir=$tmp_dir/$folder_name
mkdir $tmp_rundir

if [[ "$run_type" == "optimization" ]]; then
    run_dir=$tmp_dir/$folder_name/optimization
    mkdir -p $run_dir
fi

if [[ "$run_type" == "single" ]]; then
    run_dir=$tmp_dir/$folder_name/single_step
    mkdir -p $run_dir
fi
echo "Simulation will be run in : $tmp_rundir"
pool=$RANDOM
echo $tmp_rundir $name $pool >> $outputdir/running_directory_$folder_name

# copy files to scratch and go into scratch
cp $files input_file.ini analysis.job stopos.job $run_dir
echo "Files copied to running directory: $run_dir"
cd $run_dir

# create initial set of parameters and adding them to the stopos pool
stopos create -p $pool
echo "Stopos pool $pool created"
create_initial_pool.py  --input_file $input_file || exit
echo "Initial set of parameter created: param_set_0"
stopos -p $pool add param_set_0
echo "Parameters in param_set_0 added to pool $pool"
stopos -p $pool status

# SINGLE RUN
if [[ "$run_type" == "single" ]]; then
    if [[ $nodes == 1 ]]; then
        echo "Submitting single job"
        J_ID="$(sbatch --export=ALL,pool=$pool,venvdir=$venvdir,node=1,queue=$queue stopos.job | awk '{print $4}')"
        echo $J_ID >> $outputdir/jobs_ids_$folder_name
        echo "Submitting analysis/archiving job"
        J_ID="$(sbatch --dependency=afterok:$J_ID --export=ALL,foldername=$folder_name,analyseprogram=$analysis_program,optstep=0,tmpdir=$tmp_dir,pool=$pool,optsteps=0,venvdir=$venvdir,output=$outputdir,workdir=$workdir,sjobid=$J_ID analysis.job | awk '{print $4}')"
        echo $J_ID >> $outputdir/jobs_ids_$folder_name
    elif [[ $nodes -gt 1 ]]; then
        for j in $(seq 1 $nodes)
        do 
            J_ID="$(sbatch --export=ALL,pool=$pool,venvdir=$venvdir,node=1,queue=$queue stopos.job | awk '{print $4}')"
            echo $J_ID >> $outputdir/jobs_ids_$folder_name
            array_jobids="${array_jobids}:${J_ID}"
        done
        J_ID="$(sbatch --dependency=afterok$array_jobids --export=ALL,foldername=$folder_name,analyseprogram=$analysis_program,optstep=0,tmpdir=$tmp_dir,pool=$pool,optsteps=0,venvdir=$venvdir,output=$outputdir,workdir=$workdir,sjobid=$array_jobids analysis.job | awk '{print $4}')"
    fi
fi
# end SINGLE RUN


time_stamp=$(date +'%Y-%m-%d_%H.%M')
echo $time_stamp
# OPTIMIZATION LOOP
opt_steps=$[$opt_steps -1]
if [[ "$run_type" == "optimization" ]]; then
    echo "Submitting jobs"
    if [[ $nodes == 1 ]]; then
    for i in $(seq 0 $opt_steps)
    do
    # make directories structure
        mkdir -p opt_step\_$i
        cp $files input_file.ini stopos.job analysis.job opt_step\_$i
        echo "Files copied to opt_step_$i"
        opt_name=$run_dir/opt_step\_$i
        cd $opt_name
    	if [[ $i == 0 ]]; then
    		echo "Submitting job opt_step_$i"
            J_ID="$(sbatch --export=ALL,pool=$pool,venvdir=$venvdir,node=1,queue=$queue stopos.job | awk '{print $4}')"
        	echo $J_ID >> $outputdir/jobs_ids_$folder_name
    		echo "Submitting analysis/archiving job for opt_step_$i"
        	J_ID="$(sbatch --dependency=afterok:$J_ID --export=ALL,foldername=$folder_name,analyseprogram=$analysis_program,optstep=$i,optsteps=$opt_steps,tmpdir=$tmp_dir,pool=$pool,sstoposdir=$sstoposdir,venvdir=$venvdir,output=$outputdir,workdir=$workdir,sjobid=$J_ID analysis.job | awk '{print $4}')"
        	echo $J_ID >> $outputdir/jobs_ids_$folder_name

   		elif [[ $i > 0 ]]; then
    		echo "Submitting job opt_step_$i"
        	J_ID="$(sbatch --dependency=afterok:$J_ID --export=ALL,pool=$pool,venvdir=$venvdir,node=1,queue=$queue stopos.job | awk '{print $4}')"
        	echo $J_ID >> $outputdir/jobs_ids_$folder_name
    		echo "Submitting analysis/archiving job for opt_step_$i"
        	J_ID="$(sbatch --dependency=afterok:$J_ID --export=ALL,foldername=$folder_name,analyseprogram=$analysis_program,optstep=$i,optsteps=$opt_steps,tmpdir=$tmp_dir,pool=$pool,sstoposdir=$sstoposdir,venvdir=$venvdir,output=$outputdir,workdir=$workdir,sjobid=$J_ID analysis.job | awk '{print $4}')"
        	echo $J_ID >> $outputdir/jobs_ids_$folder_name
   		fi
   		cd $run_dir
   done
   elif [[ $nodes -gt 1 ]]; then
        for i in $(seq 0 $opt_steps)
        do
        # make directories structure
            mkdir -p opt_step\_$i
            cp $files input_file.ini stopos.job analysis.job opt_step\_$i
            echo "Files copied to opt_step_$i"
            opt_name=$run_dir/opt_step\_$i
            cd $opt_name
        	if [[ $i == 0 ]]; then
        		echo "Submitting job opt_step_$i"
                for j in $(seq 1 $nodes)
                do 
            	    J_ID="$(sbatch --export=ALL,pool=$pool,venvdir=$venvdir,node=$j stopos.job | awk '{print $4}')"
            	    echo $J_ID >> $outputdir/jobs_ids_$folder_name
                    array_jobids="${array_jobids}:${J_ID}"
                done
        		echo "Submitting analysis/archiving job for opt_step_$i"
            	J_ID="$(sbatch --dependency=afterok$array_jobids --export=ALL,foldername=$folder_name,analyseprogram=$analysis_program,optstep=$i,optsteps=$opt_steps,tmpdir=$tmp_dir,pool=$pool,sstoposdir=$sstoposdir,venvdir=$venvdir,output=$outputdir,workdir=$workdir,sjobid=$array_jobids analysis.job | awk '{print $4}')"
            	echo $J_ID >> $outputdir/jobs_ids_$folder_name

        	elif [[ $i > 0 ]]; then
        		echo "Submitting job opt_step_$i"
                for j in $(seq 1 $nodes)
                do 
            	    J_ID="$(sbatch --dependency=afterok:$J_ID --export=ALL,pool=$pool,venvdir=$venvdir,node=$j stopos.job | awk '{print $4}')"
            	    echo $J_ID >> $outputdir/jobs_ids_$folder_name
                    array_jobids="${array_jobids}:${J_ID}"
        	    done
        	    echo "Submitting analysis/archiving job for opt_step_$i"
                J_ID="$(sbatch --dependency=afterok$array_jobids --export=ALL,foldername=$folder_name,analyseprogram=$analysis_program,optstep=$i,optsteps=$opt_steps,tmpdir=$tmp_dir,pool=$pool,sstoposdir=$sstoposdir,venvdir=$venvdir,output=$outputdir,workdir=$workdir,sjobid=$array_jobids analysis.job | awk '{print $4}')"
        	    echo $J_ID >> $outputdir/jobs_ids_$folder_name
            fi
            unset array_jobids
   		cd $run_dir
        done
    fi
fi
#  end OPTIMIZATION LOOP
echo "All jobs submitted; jobids stored in jobs_ids file; information about
running directory in runnning_directory file"
time_stamp=$(date +'%Y-%m-%d_%H.%M')
echo $time_stamp
echo $time_stamp
