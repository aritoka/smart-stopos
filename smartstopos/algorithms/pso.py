"""Implementation of PSO"""
from __future__ import division
import random
import sys
import os


class Particle:
    """Defines the particles and how they evolve through time.

    Parameters
    ----------
    x0: array?
        array with the fitness x0[0] and the evaluated point x0[1:]

    Returns
    -------
    new_generation: array of array
        New set of data point to evaluate

    Notes
    -----
    PSO need information of previous steps. For this we recursively check the last
    fitness function values and the global best. This is read from a file.

    """

    def __init__(self, x0, num_dimensions):
        self.position_i = []          # particle position
        self.velocity_i = []          # particle velocity
        self.pos_best_i = []          # best position individual
        # self.err_best_i=-1          # best error individual
        self.err_i = -1.0               # error individual

        for i in range(0, num_dimensions):
            self.velocity_i.append(random.uniform(-1, 1))
            self.position_i.append(x0[i])

        self.pos_best_i = self.position_i
        self.err_best_i = self.err_i

    def evaluate(self, f, i):
        self.err_i = f
        # check to see if the current position is an individual best
        if self.err_i < self.all_best[i] or self.err_best_i == -1:
            self.pos_best_i = self.position_i
            self.err_best_i = self.err_i

    # update new particle velocity
    def update_velocity(self, pos_best_g, num_dimensions):
        w = 0.5       # constant inertia weight (how much to weigh the previous velocity)
        c1 = 1        # cognitive constant
        c2 = 2        # social constant

        for i in range(0, num_dimensions):
            r1 = random.random()
            r2 = random.random()

            vel_cognitive = c1*r1*(self.pos_best_i[i]-self.position_i[i])
            vel_social = c2*r2*(pos_best_g[i]-self.position_i[i])
            self.velocity_i[i] = w*self.velocity_i[i]+vel_cognitive+vel_social

    # update the particle position based off new velocity updates
    def update_position(self, set_param, num_dimensions):
        for i in range(0, num_dimensions):
            print(set_param.parameters.items[i])
            self.position_i[i] = self.position_i[i]+self.velocity_i[i]

            # adjust maximum position if necessary
            if self.position_i[i] > set_param.parameters[i].range[0]:
                self.position_i[i] = set_param.parameters[i].range[0]

            # adjust minimum position if necessary
            if self.position_i[i] < set_param.parameters[i].range[1]:
                self.position_i[i] = set_param.parameters[i].range[1]


def particle_swarm_opt(data, set_param, sim_param, opt_step):
    num_particles = len(data)
    num_dimensions = len(set_param.parameters)
    pos_best_g = []
    if opt_step >= 0:
        prev_step = opt_step - 1
        prev_file = 'opt_step_'+str(prev_step)+'/csv_output.csv'
        if os.path.exists(prev_file):
            with open(prev_file, 'r') as f:
                try:
                    fitness_previous_step = []
                    positions_previous_step = []
                    for line in f:
                        fitness_previous_step.append(line.split(',')[0])
                        positions_previous_step.append([line.split(',')[1:]])
                    err_best_g = fitness_previous_step[0]  # just to put a number
                    for i, item in enumerate(fitness_previous_step):
                        if sim_param['maximum'] is True:
                            if item > err_best_g:
                                err_best_g = float(item)
                                pos_best_g = positions_previous_step[i]
                                # print (err_best_g)
                                # print (pos_best_g)
                        if sim_param['maximum'] is not True:
                            if item < err_best_g:
                                err_best_g = float(item)
                                pos_best_g = positions_previous_step[i]
                                # print (err_best_g)
                                # print (pos_best_g)
                except OSError:
                    print("Could not find file previous opt step")
                    sys.exit()

    # Build swarm
    swarm = []
    for item in data:
        swarm.append(Particle(item[1:], num_dimensions))

    # Begin optimization loop
    for j in range(0, num_particles):
        # determine if current particle is the best (globally)
        # pos_previous = positions_previous_step[j]
        if swarm[j].err_i < err_best_g or err_best_g == -1:
            pos_best_g = swarm[j].position_i
            print("best global")
            print(pos_best_g)
            err_best_g = float(swarm[j].err_i)

        # cycle through swarm and update velocities and position
        for k in range(0, num_particles):
            swarm[k].update_velocity(pos_best_g, num_dimensions)
            swarm[k].update_position(set_param, num_dimensions)

    new_generation = []
#    for particle in swarm:
#        new_generation.append(particle.position)
    return new_generation
