""" Implmeentation of genetic algorithm for N-parameters."""
import random
import numpy as np


def best_candidates(data, number_parents, maximum):
    """Selects the best data points.

    Parameters
    ----------
    data : list
        list of the data points (list) from output file.
    number_parents : int
        Number of candidates around which new data points will be created.
    maximum : bool
        Whether we want to find the minimum or maximum.

    Returns
    -------
    best_data_points: list of arrays
        Returns the fittest n (number_parents) sets of parameters.
    """
    if not isinstance(data, list):
        raise TypeError("Data most be a list")

    # Sort data based on first column
    sort = np.argsort([item[0] for item in data])
    if maximum:
        sort = sort[::-1]
    data_sorted = np.array(data)[sort]
    best_data_points = []
    proba = 1.0  # proba adding a parent?
    for item in data_sorted:
        if random.random() < proba and len(best_data_points) < number_parents:
            best_data_points.append(item[1:])
    return best_data_points


def gaussians(data, set_param, sim_param, opt_step):
    """Creates a new data points drawn from a gaussian distribution around best
    candidates.

    Parameters
    ----------
    data : list of np.ndarrays
        data from output file
    set_param : 'smarstopos/parameters/SetParameter'
        Set of parameters ot be explored in the simulation.
    sim_param : dict
        Dictionary with the information of the simulation.
    opt_step : int
        Current optimization step.

    Returns
    -------
    new_data_points : list or arrays
        New set of data point to be explored
    """
    if not isinstance(data, list):
        raise TypeError("Data is not a list")

    best_data_points = best_candidates(data, sim_param['number_best_candidates'],
                                       sim_param['maximum'])
    gaussian_points = []
    sim_param['global_width_distribution'] = sim_param['global_width_distribution']/(1.0 + opt_step)
    print("Width gaussian for this opt_step: {}".format(sim_param['opt_steps']))

    for i, point in enumerate(best_data_points):
        for _ in range(0, 5):
            for j, param in enumerate(set_param.parameters.values()):
                new_point = point
                param.width_distribution = param.width_distribution/(1.0 + opt_step)
                while True:
                    new_point[j] = np.random.normal(point[j],
                                                    param.width_distribution)
                    if new_point[j] > param.range[0] and new_point[j] < param.range[1]:
                        if param.data_type == 'discrete':
                            gaussian_points.append(np.array(new_point, dtype=int))
                        else:
                            gaussian_points.append(np.array(new_point, dtype=float))
                        break
    return gaussian_points
