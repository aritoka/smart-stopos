#!/usr/bin/env python3
"""
Creates a param_set files to be added to the stopos pool based on the parameters
input file.

Usage:
    python create_initial_pool.py --input_file input_file.ini
"""
from argparse import ArgumentParser
import random
import logging

from smartstopos.utils.stopostools import create_stopos_file
from smartstopos.utils.stopostools import create_stopos_file_random
from smartstopos.utils.readinput import read_input
from smartstopos.utils.makedatapoints import make_data_points
from smartstopos.utils.makedatapoints_random import make_data_points_random


def main(input_file):
    """Function to create an initial pool of parameters based on input file."""

    logging.basicConfig(filename='sim.log', level=logging.DEBUG)
    # initialize default sim_param:
    sim_param = {'number_best_candidates': 10, 'proba_mutation': 1.0,
                 'proba_crossover': 1.0, 'global_scale_factor': 1.0, 'maximum':
                 False, 'seed': random.random(), 'run_type': 'single',
                 'algorithm': None, 'distribution': None}

    # read the parameters input file and create set of parameters to explore
    set_param = read_input(input_file, sim_param)
    if sim_param['distribution'] == 'fully_random':
        list_points = make_data_points_random(sim_param, set_param)
    else:
        make_data_points(set_param)

    # create initial param_set file to be used by stopos
    opt_step = 0
    print("Current optimization step: {}".format(opt_step))
    logging.debug("Simulation details:")
    for item in sim_param.items():
        logging.debug(item)
    logging.debug("Parameters to explore:")
    for name, parameter in set_param.parameters.items():
        logging.debug("name: {} --> range: {}, number_points: {}, distribution: {}, data_type: {}, scale_factor: {};"
                      "width_distribution: {}; weight: {}".format(name,
                                                                  parameter.range,
                                                                  parameter.number_points,
                                                                  parameter.distribution,
                                                                  parameter.data_type,
                                                                  parameter.scale_factor,
                                                                  parameter.width_distribution,
                                                                  parameter.weight))
    if sim_param['distribution'] == 'fully_random':
        create_stopos_file_random(sim_param, set_param, list_points, number_runs=1, name=str(opt_step))
    else:
        create_stopos_file(sim_param, set_param, number_runs=1, name=str(opt_step))


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--input_file', required=False, default="input_file.ini", type=str,
                        help='File with the input information about the simulation and parameters')
    args = parser.parse_args()
    main(args.input_file)
