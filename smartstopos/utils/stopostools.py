"""
Create input files for STOPOS.
"""
import itertools
from smartstopos.utils.parserconstraints import evaluate_constraints


def create_opt_stopos_file(set_param, number_runs=1, name=''):
    """Creates a file to be used by STOPOS.

    Parameters
    ----------
    set_param : smartstopos.utils.parameters.SetParameters
        Set of parameters to be explored during the simulations. Data points of
        the simulation are created from these parameters.
    number_runs: int
        Number of times to run mutations and crossover in the GA algorithm. The
        default value is one, a higher values increases the size of the next
        data set to be simulated.
    name : str
        Name for the file with the data points for the next simulations. In
        general the name is the optimization step to be perform next.
    """
    f = open("param_set_" + name, "w")

    len_data_points = len(list(set_param.parameters.values())[0].data_points)
    for point in range(0, len_data_points):
        for _, param in set_param.parameters.items():
            if param.data_type == 'discrete':
                f.write("{} \t".format(int(param.data_points[point])))
            elif param.data_type == 'continuous':
                f.write("{} \t".format(param.data_points[point]))
            else:
                raise ValueError("data type of one of the parameters does not exist")
        f.write("\n")
    f.close()


def create_stopos_file(sim_param, set_param, number_runs=1, name=''):
    """Creates a file to be used by STOPOS.

    Parameters
    ----------
    sim_params : dict
        Dictionary with the simulation details read from the input file.
    set_param : smartstopos.utils.parameters.SetParameters
        Set of parameters to be explored during the simulations. Data points of
        the simulation are created from these parameters.
    number_runs: int
        Number of times to run mutations and crossover in the GA algorithm. The
        default value is one, a higher values increases the size of the next
        data set to be simulated.
    name : str
        Name for the file with the data points for the next simulations. In
        general the name is the optimization step to be perform next.
    """
    all_data_points = []
    for _key, param in set_param.parameters.items():
        if param.data_type == "discrete":
            param.data_points = list(map(int, param.data_points))
            all_data_points.append(param.data_points)
        elif param.data_type == 'continuous':
            all_data_points.append(param.data_points)
        else:
            raise ValueError("data type of one of the parameters does not exist")
    points = []
    for item in itertools.product(*all_data_points):
        points.append(item)
    if 'constraints' in sim_param.keys():
        print("\nChecking constraints...")
        new_points = evaluate_constraints(sim_param, set_param, points)
        if len(new_points) != len(points):
            print("Some global constraints are not satisfied, check your initial parameter settings")
    else:
        new_points = points
    f = open("param_set_" + name, "w")
    for item in new_points:
        for _ in range(0, number_runs):
            for value in item:
                if isinstance(value, int):
                    f.write("{}\t".format(value))
                else:
                    f.write("{}\t".format(value))
            f.write("\n")
    f.close()


def create_stopos_file_random(sim_param, set_param, list_points, number_runs=1, name=''):
    """Create a file to be used by STOPOS.

    Parameters
    ----------
    sim_params : dict
        Dictionary with the simulation details read from the input file.
    set_param : smartstopos.utils.parameters.SetParameters
        Set of parameters to be explored during the simulations. Data points of
        the simulation are created from these parameters.
    list_points: list
        List of random N-dimensional (N=number parameters) vectors
    number_runs: int
        Number of times to run mutations and crossover in the GA algorithm. The
        default value is one, a higher values increases the size of the next
        data set to be simulated.
    name : str
        Name for the file with the data points for the next simulations. In
        general the name is the optimization step to be perform next.
    """
    f = open("param_set_" + name, "w")

    is_discrete = [param.data_type == 'discrete' for param in set_param.parameters.values()]

    for item in list_points:
        for _ in range(0, number_runs):
            for i, value in enumerate(item):
                if is_discrete[i]:
                    f.write("{:d}\t".format(int(value)))
                else:
                    f.write("{}\t".format(value))
            f.write("\n")
    f.close()
