"""Read input file of smartopos.s"""
from smartstopos.utils.parameters import Parameter, SetParameters


def str2bool(v):
    return v.lower() in ("True", "yes", "true", "t", "1")


def read_input(filename, sim_parameters):
    """Read input file for smartstopos.

    Parameters
    ----------
    filename : str
        Name of the input file
    sim_parameters : dict
        A dictionary with the parameters needed for the simulations as read from
        input file.

    Returns
    -------
    set_parameters : smartstopos.utils.parameter.SetParameters
        Set of parameters and their properties as read from the input file.
    """
    parameters = False
    set_parameters = SetParameters()
    sim_info = False

    # defaults for some optimization algorithms. Still needed?
    scale_factor = 1.0
    width_distribution = 1.0
    p_weight = 1.0
    sim_parameters['global_width_distribution'] = 1.0
    sim_parameters['global_scale_factor'] = 1.0

    with open(filename, "r") as f:
        lines = [line.strip() for line in f.readlines()]
    for line in lines:
        variable = line.split(":")[0].strip()
        if line == 'GENERAL':
            sim_info = True
        elif line == 'END_GENERAL':
            sim_info = False
        if sim_info:
            if variable == 'run_type':
                value = line.split(":")[1]
                if value.split()[0] not in ["single", "optimization"]:
                    raise ValueError("Run type error. Please chose between single or optimization")
                else:
                    sim_parameters['run_type'] = value.split()[0]

                if value.split()[0] == 'optimization':
                    if len(value.split()) < 3:
                        raise ValueError("run_type optimization minimal arguments:\n run_type: "
                                         "optimization algorithm opt_steps\n If not specified, "
                                         "it will perform a normal selection mechanism and corssover before mutation.")
                    sim_parameters['algorithm'] = value.split()[1]
                    sim_parameters['opt_steps'] = int(value.split()[2])
                    if len(value.split()) > 3:
                        if value.split()[3] == 'c':
                            sim_parameters['c'] = True
                        elif value.split()[3] == 'm':
                            sim_parameters['m'] = True
                        elif value.split()[3] == 'cr':
                            sim_parameters['c'] = True
                            sim_parameters['roulette'] = True
                        elif value.split()[3] == 'mr':
                            sim_parameters['m'] = True
                            sim_parameters['roulette'] = True
                    # default GA is c
                    else:
                        sim_parameters['c'] = True
                    if 'roulette' not in sim_parameters:
                        sim_parameters['roulette'] = False

            elif variable == 'global_scale_factor':
                value = float(line.split(":")[1].strip())
                sim_parameters['global_scale_factor'] = value
                scale_factor = sim_parameters['global_scale_factor']
            elif variable == 'distribution':
                value = line.split(":")[1].strip()
                if value == "fully_random":
                    print("WARNING: using fully_random in the general settings overwrites "
                          "any distribution choice at parameters level")
                sim_parameters['distribution'] = value
            elif variable == 'proba_mutation':
                value = float(line.split(":")[1].strip())
                sim_parameters['proba_mutation'] = value
            elif variable == 'proba_crossover':
                value = float(line.split(":")[1].strip())
                sim_parameters['proba_crossover'] = value
            elif variable == 'number_parameters':
                value = int(line.split(":")[1].strip())
                sim_parameters['number_parameters'] = value
            elif variable == 'maximum' or variable == 'Maximum':
                sim_parameters['maximum'] = str2bool(str(line.split(":")[1].strip()))
            elif variable == 'seed':
                sim_parameters['seed'] = float(line.split(":")[1].strip())
            elif variable == 'number_best_candidates':
                sim_parameters['number_best_candidates'] = int(line.split(":")[1].strip())
            elif variable == 'global_width_distribution':
                sim_parameters['global_width_distribution'] = float(line.split(":")[1].strip())
                width_distribution = sim_parameters['global_width_distribution']
            elif variable == 'population_size':
                print("WARNING: population size does not apply in single runs or first optimization step")
                value = int(line.split(":")[1].strip())
                sim_parameters['population_size'] = value
            elif variable == 'constraints':
                value = line.split(":")[1].strip()
                sim_parameters['constraints'] = compile(value, 'constraints', 'eval')

        if variable == 'Parameter':
            parameters = True
            minimal_requirements = {'min': False,
                                    'max': False,
                                    'num_points': False,
                                    'distribution': False,
                                    'type': False
                                    }
            width_distribution = sim_parameters['global_width_distribution']
            scale_factor = sim_parameters['global_scale_factor']
            p_weight = 1.0
        if parameters:
            if variable == 'name':
                name = str(line.split(":")[1].strip())
            elif variable == 'max':
                minimal_requirements['max'] = True
                maxr = float(line.split(":")[1].strip())
            elif variable == 'min':
                minimal_requirements['min'] = True
                minr = float(line.split(":")[1].strip())
            elif variable == 'number_points':
                minimal_requirements['num_points'] = True
                num_points = int(line.split(":")[1].strip())
            elif variable == 'distribution':
                dist = str(line.split(":")[1].strip())
                minimal_requirements['distribution'] = True
                valid_distributions = ["uniform", "log", "random", "normal"]
                if dist not in valid_distributions:
                    raise ValueError("Distribution not define. "
                                     "Please chose an existing one or define your own in makedatapoints.py")
            elif variable == 'type':
                typep = str(line.split(":")[1].strip())
                if typep not in ["discrete", "continuous"]:
                    raise ValueError("Data type of parameter {} is not valid. "
                                     "Please chose between discrete and continuous".format(name))
                minimal_requirements['type'] = True
            elif variable == 'scale_factor':
                scale_factor = float(line.split(":")[1].strip())
            elif variable == 'width_distribution':
                width_distribution = float(line.split(":")[1].strip())
            elif variable == 'weight':
                p_weight = float(line.split(":")[1].strip())

        if parameters and line.rstrip() == 'end':
            if minr > maxr:
                raise ValueError("The minimum value is larger than the maximum for parameter '{}'".format(name))
            if all(minimal_requirements):
                set_parameters.add_parameter(Parameter(name, [minr, maxr],
                                             num_points, typep, dist, scale_factor,
                                             width_distribution, variance=0.0,
                                             weight=p_weight, active=True))

            else:
                raise ValueError("One or more of the minimal requirements (min, max, number_point, type, dsitribution) "
                                 "for parameter {} is not defined".format(name))
            parameters = False
    return set_parameters
