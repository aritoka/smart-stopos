"""Unit tests for ga algorithm."""
import os
import unittest
import random
from smartstopos.utils.makedatapoints import make_data_points
from smartstopos.utils.parameters import SetParameters, Parameter
from smartstopos.utils.stopostools import create_stopos_file


class TestStoposTools(unittest.TestCase):
    """Unit tests for input reading."""

    def test_create_file(self):
        set_param = SetParameters()
        set_param.add_parameter(Parameter('p1', [0.0, 1.0], 9, 'continuous',
                                          'uniform', 1.0, 1.0, variance=0.0, weight=1.0, active=True))
        set_param.add_parameter(Parameter('p2', [0.0, 3.0], 4, 'discrete',
                                          'uniform', 1.0, 0.5, variance=0.0, weight=1.0, active=False))
        set_param.add_parameter(Parameter('p3', [0, 1], 9, 'continuous',
                                          'uniform', 1.0, 0.5, variance=0.0, weight=1.0, active=False))
        set_param.add_parameter(Parameter('p4', [0.0, 3.0], 6, 'discrete',
                                          'uniform', 1.0, 0.5, variance=0.0, weight=1.0, active=False))

        make_data_points(set_param)
        self.assertEqual(set_param.parameters['p1'].data_points[0], 0.0)
        self.assertEqual(set_param.parameters['p1'].data_points[6], 0.75)
        self.assertEqual(len(set_param.parameters['p1'].data_points), 9)
        self.assertEqual(set_param.parameters['p2'].data_points[0], 0)
        self.assertEqual(set_param.parameters['p2'].data_points[3], 3)
        self.assertEqual(set_param.parameters['p3'].data_points[0], 0.0)
        self.assertEqual(set_param.parameters['p3'].data_points[6], 0.75)
        self.assertEqual(set_param.parameters['p4'].data_points[0], 0)
        self.assertEqual(set_param.parameters['p4'].data_points[1], 1)
        # print (set_param.parameters['p4'].data_points)
        self.assertEqual(set_param.parameters['p4'].data_points[2], 2)
        self.assertEqual(set_param.parameters['p4'].data_points[3], 2)
        set_param.remove_parameter('p3')
        set_param.remove_parameter('p4')
        sim_param = {'number_best_candidates': 10,
                     'proba_mutation': 1.0,
                     'proba_crossover': 1.0,
                     'global_scale_factor': 1.0,
                     'global_width_distribution': 1.0,
                     'maximum': False,
                     'seed': random.random(),
                     'algorithm': None,
                     'c': True, 'm': False}
        create_stopos_file(sim_param, set_param, name='test')
        with open("param_set_test", "r") as fp:
            lines = [line.strip().split('\t') for line in fp.readlines()]
        total = set_param.parameters['p1'].number_points*set_param.parameters['p2'].number_points
        self.assertEqual(len(lines), total)
        self.assertEqual(float(lines[0][0]), set_param.parameters['p1'].range[0])
        self.assertEqual(float(lines[total-1][0]), set_param.parameters['p1'].range[1])
        self.assertEqual(float(lines[0][1]), set_param.parameters['p2'].range[0])
        self.assertEqual(float(lines[total-1][1]), set_param.parameters['p2'].range[1])
        os.remove('param_set_test')


if __name__ == '__main__':
    unittest.main()
