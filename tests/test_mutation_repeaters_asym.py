import os
import random
from numpy import array
from itertools import product
from smartstopos.utils.readinput import read_input
from smartstopos.utils.makedatapoints import make_data_points
from smartstopos.algorithms.ga import mutate

test_input_file = "test_input.ini"


def test_mutate_asym_degree_n_repeaters():
    """Tests that if one of these two parameters is mutated, the other isn't.
    By setting `proba_mutation` to 1. and choosing the appropriate seed, we guarantee that the first of the parameters
    (`n_repeaters` in the configuration file) is always mutated, while the second one never is. We do not however assert
    that the first parameter's value changes, because it might that the mutation rounds to 0 due to the parameters being
    integer-valued.
    """
    dir_ = os.path.dirname(os.path.realpath(__file__)) + "/"
    input_file = dir_ + test_input_file
    sim_param = {'number_best_candidates': 2, 'proba_mutation': 1.0,
                 'proba_crossover': 1.0, 'global_scale_factor': 1.0, 'maximum':
                 False, 'run_type': 'single',
                 'algorithm': None, 'distribution': None}
    set_parameters = read_input(input_file, sim_param)
    sim_param["population_size"] = 2

    # verified empirically that choosing this seed ensures that we mutate each of the two points once
    random.seed(2)
    assert random.sample(range(2), 1)[0] == 0
    make_data_points(set_parameters)
    separate_data_points = [parameter.data_points for parameter in set_parameters.parameters.values()]
    joint_data_points = [array(elem) for elem in list(product(separate_data_points[0], separate_data_points[1]))]
    mutated_points = mutate(joint_data_points, set_parameters, sim_param, [])
    for i in range(len(joint_data_points)):
        assert joint_data_points[i][1] == mutated_points[i][1]
