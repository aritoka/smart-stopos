"""
Unit tests for initial points distribution
"""
import unittest
import os
from smartstopos.utils.readinput import read_input
from smartstopos.utils.parameters import *
from smartstopos.utils.stopostools import *
from smartstopos.utils.makedatapoints import *
from smartstopos.pools.create_initial_pool import *
from smartstopos.pools.create_opt_pool import *


class TestDistribution(unittest.TestCase):
    """
    Unit tests for uniform and log distribution

    """
    def test_distribution_uniform(self):
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2 m\n')
        f.write('maximum: True\n')
        f.write('proba_crossover: 1.0\n')
        f.write('proba_mutation: 1.0\n')
        f.write('global_scale_factor: 1.0\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param ={'number_best_candidates':10,
                    'proba_mutation': 1.0,
                    'proba_crossover': 1.0,
                    'global_scale_factor': 1.0,
                    'global_width_distribution': 1.0,
                    'maximum': False,
                    'seed': random.random(),
                    'algorithm': None,
                    'c': True, 'm': False}
        # test input
        set_param = read_input('input_file.ini', sim_param)
        self.assertEqual(len(set_param.parameters), 1)
        self.assertEqual(set_param.parameters['x'].data_type, "discrete")
        self.assertEqual(set_param.parameters['x'].range[0], -4)
        self.assertEqual(set_param.parameters['x'].range[1], 4)
        self.assertEqual(set_param.parameters['x'].distribution, "uniform")
        make_data_points(set_param)
        self.assertEqual(set_param.parameters['x'].data_points[0], -4)
        self.assertEqual(len(set_param.parameters['x'].data_points), 9)
        self.assertEqual(bool(set(set_param.parameters['x'].data_points).intersection([-4,-3,-2,-1,0,1,2,3,4])), True)
        self.assertEqual(set_param.parameters['x'].data_points[8], 4)
        os.remove('input_file.ini')
    
    def test_distribution_log(self):
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2 m\n')
        f.write('maximum: True\n')
        f.write('proba_crossover: 1.0\n')
        f.write('proba_mutation: 1.0\n')
        f.write('global_scale_factor: 1.0\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:1\n')
        f.write('max:1000\n')
        f.write('number_points:4\n')
        f.write('distribution:log\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param ={'number_best_candidates':10,
                    'proba_mutation': 1.0,
                    'proba_crossover': 1.0,
                    'global_scale_factor': 1.0,
                    'global_width_distribution': 1.0,
                    'maximum': False,
                    'seed': random.random(),
                    'algorithm': None,
                    'c': True, 'm': False}
        set_param = read_input('input_file.ini', sim_param)
        self.assertEqual(set_param.parameters['x'].distribution, "log")
        make_data_points(set_param)
        self.assertEqual(set_param.parameters['x'].data_points[0], 1)
        self.assertEqual(set_param.parameters['x'].data_points[3], 1000)
        self.assertEqual(bool(set(set_param.parameters['x'].data_points).intersection([1, 10, 100, 1000])), True)
        self.assertEqual(len(set_param.parameters['x'].data_points), 4)
        os.remove('input_file.ini')
    
    def test_distribution_log_uniform(self):
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2\n')
        f.write('proba_crossover: 0.5\n')
        f.write('proba_mutation:0.5\n')
        f.write('global_scale_factor:1.0\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor: 0.6\n')
        f.write('type:discrete\n')
        f.write('weight:0.5\n')
        f.write('constraint: no\n')
        f.write('end\n')
        f.write('Parameter: 2\n')
        f.write('name:y\n')
        f.write('min:1\n')
        f.write('max:1000\n')
        f.write('number_points:4\n')
        f.write('distribution: log\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param ={'number_best_candidates':10,
                    'proba_mutation': 1.0,
                    'proba_crossover': 1.0,
                    'global_scale_factor': 1.0,
                    'global_width_distribution': 1.0,
                    'maximum': False,
                    'seed': random.random(),
                    'algorithm': None,
                    'c': True, 'm': False}
        set_param = read_input('input_file.ini', sim_param)
        self.assertEqual(set_param.parameters['x'].data_type, "discrete")
        self.assertEqual(set_param.parameters['y'].data_type, "discrete")
        self.assertEqual(set_param.parameters['x'].distribution, "uniform")
        self.assertEqual(set_param.parameters['y'].distribution, "log")
        make_data_points(set_param)
        self.assertEqual(set_param.parameters['x'].data_points[0], -4)
        self.assertEqual(len(set_param.parameters['x'].data_points), 9)
        self.assertEqual(set_param.parameters['y'].data_points[0], 1)
        self.assertEqual(set_param.parameters['y'].data_points[3], 1000)
        self.assertEqual(bool(set(set_param.parameters['y'].data_points).intersection([1, 10, 100, 1000])), True)
        self.assertEqual(len(set_param.parameters['y'].data_points), 4)
    
if __name__ == '__main__':
    unittest.main()
