"""Unit tests for constraints define in input file."""
import unittest
import os
import random
import itertools

from smartstopos.utils.readinput import read_input
from smartstopos.utils.parserconstraints import evaluate_constraints
from smartstopos.utils.makedatapoints import make_data_points


class TestConstraints(unittest.TestCase):
    """Unit tests for uniform and log distribution."""

    def test_constraints1(self):
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2 m\n')
        f.write('maximum: True\n')
        f.write('proba_crossover: 1.0\n')
        f.write('proba_mutation: 1.0\n')
        f.write('global_scale_factor: 1.0\n')
        f.write('constraints:x<3 and x+2<2\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param = {
            'number_best_candidates': 10,
            'proba_mutation': 1.0,
            'proba_crossover': 1.0,
            'global_scale_factor': 1.0,
            'global_width_distribution': 1.0,
            'maximum': False,
            'seed': random.random(),
            'algorithm': None,
            'c': True,
            'm': False
        }
        set_param = read_input('input_file.ini', sim_param)
        make_data_points(set_param)
        all_data_points = []
        for _key, param in set_param.parameters.items():
            if param.data_type == "discrete":
                param.data_points = map(int, param.data_points)
                all_data_points.append(param.data_points)
            elif param.data_type == 'continuous':
                all_data_points.append(param.data_points)
        points = [item for item in itertools.product(*all_data_points)]
        self.assertEqual(len(points), 9)
        print(points)
        if 'constraints' in sim_param.keys():
            new_points = evaluate_constraints(sim_param, set_param, points)
        self.assertEqual(len(new_points), 4)
        os.remove('input_file.ini')

    def test_constraints2(self):
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2 m\n')
        f.write('maximum: True\n')
        f.write('proba_crossover: 1.0\n')
        f.write('proba_mutation: 1.0\n')
        f.write('global_scale_factor: 1.0\n')
        f.write('constraints:x<3 and y>0 and y+x<12\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('Parameter: 2\n')
        f.write('name:y\n')
        f.write('min:1\n')
        f.write('max:10\n')
        f.write('number_points:10\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param = {
            'number_best_candidates': 10,
            'proba_mutation': 1.0,
            'proba_crossover': 1.0,
            'global_scale_factor': 1.0,
            'global_width_distribution': 1.0,
            'maximum': False,
            'seed': random.random(),
            'algorithm': None,
            'c': True,
            'm': False
        }
        set_param = read_input('input_file.ini', sim_param)
        make_data_points(set_param)
        all_data_points = []
        for _key, param in set_param.parameters.items():
            if param.data_type == "discrete":
                param.data_points = map(int, param.data_points)
                all_data_points.append(param.data_points)
            elif param.data_type == 'continuous':
                all_data_points.append(param.data_points)
        points = [item for item in itertools.product(*all_data_points)]
        constraint_broke = False
        if 'constraints' in sim_param.keys():
            new_points = evaluate_constraints(sim_param, set_param, points)
        for item in new_points:
            if item[0] > 3:
                constraint_broke = True
            if item[1] < 0:
                constraint_broke = True
            if item[0] + item[1] > 12:
                constraint_broke = True
        self.assertEqual(constraint_broke, False)

    def test_constraints2(self):
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2 m\n')
        f.write('maximum: True\n')
        f.write('proba_crossover: 1.0\n')
        f.write('proba_mutation: 1.0\n')
        f.write('global_scale_factor: 1.0\n')
        f.write('constraints:abs(x) < y**2 and y*x<=0\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('Parameter: 2\n')
        f.write('name:y\n')
        f.write('min:1\n')
        f.write('max:10\n')
        f.write('number_points:10\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param = {
            'number_best_candidates': 10,
            'proba_mutation': 1.0,
            'proba_crossover': 1.0,
            'global_scale_factor': 1.0,
            'global_width_distribution': 1.0,
            'maximum': False,
            'seed': random.random(),
            'algorithm': None,
            'c': True,
            'm': False
        }
        set_param = read_input('input_file.ini', sim_param)
        make_data_points(set_param)
        all_data_points = []
        for _key, param in set_param.parameters.items():
            if param.data_type == "discrete":
                param.data_points = map(int, param.data_points)
                all_data_points.append(param.data_points)
            elif param.data_type == 'continuous':
                all_data_points.append(param.data_points)

        points = [item for item in itertools.product(*all_data_points)]

        constraint_broke = False
        if 'constraints' in sim_param.keys():
            new_points = evaluate_constraints(sim_param, set_param, points)

        for item in new_points:
            if abs(item[0]) > item[1]**2:
                constraint_broke = True
        self.assertEqual(constraint_broke, False)


if __name__ == '__main__':
    unittest.main()
