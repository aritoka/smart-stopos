"""Unit tests for ga algorithm

"""
import unittest
import os
import random
import numpy as np
from smartstopos.algorithms.ga import *
from smartstopos.utils.parameters import SetParameters, Parameter
from smartstopos.utils.readinput import read_input

sim_param_full_GA = {'proba_mutation': 1.0,
                     'proba_crossover': 1.0,
                     'global_scale_factor': 0.1,
                     'global_width_distribution': 1.0,
                     'number_best_candidates': 5,
                     'maximum': False,
                     'seed': 1.0,
                     'algorithm': 'GA',
                     'opt_steps': 2,
                     'population_size': 10,
                     'c': True, 'm': False}


def circle(x, y):
    return x**2 + y**2


def f(x):
    return x**2


class TestGA(unittest.TestCase):
    """Unit tests for the Port class."""

    def test_parents(self):
        a = [np.array([circle(1, 2), 1, 2]),
             np.array([circle(5, 6), 5, 6]),
             np.array([circle(0, 1), 0, 1]),
             np.array([circle(0, 0), 0, 0]),
             np.array([circle(2, 0), 2, 0]),
             np.array([circle(-1, 0.5), -1, 0.5])]
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2 m\n')
        f.write('maximum: False\n')
        f.write('proba_crossover: 0.5\n')
        f.write('proba_mutation: 0.5\n')
        f.write('global_scale_factor: 1.0\n')
        f.write('number_best_candidates: 2\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor: 1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param = {'proba_mutation': 1.0,
                     'proba_crossover': 1.0,
                     'global_scale_factor': 1.0,
                     'global_width_distribution': 1.0,
                     'number_best_candidates': 4,
                     'maximum': False,
                     'seed': random.random(),
                     'algorithm': None,
                     'c': True, 'm': False}
        # test input
        set_param = read_input('input_file.ini', sim_param)
        os.remove('input_file.ini')
        self.assertIsInstance(a, list)
        parents, objf, sorted_values = get_parents(a, sim_param)
        self.assertEqual(len(parents[0]), 2)
        self.assertEqual(0, parents[0][0])
        self.assertEqual(0, objf[0])
        self.assertEqual(1, parents[1][1])
        self.assertEqual(1, objf[1])
        sim_param['number_best_candidates'] = 5
        parents, objf, sorted_values = get_parents(a, sim_param)
        self.assertEqual(len(parents), 5)
        sim_param['number_best_candidates'] = 4
        parents, objf, sorted_values = get_parents(a, sim_param)
        self.assertEqual(len(parents), 4)
        sim_param['maximum'] = True
        sim_param['number_best_candidates'] = 1
        parents, objf, sorted_values = get_parents(a, sim_param)
        self.assertEqual(61, objf[0])

    def test_mutation(self):
        a = [np.array([circle(1, 2), 1, 2]),
             np.array([circle(5, 6), 5, 6]),
             np.array([circle(0, 1), 0, 1]),
             np.array([circle(0, 0), 0, 0]),
             np.array([circle(2, 0), 2, 0]),
             np.array([circle(-1, 0.5), -1, 0.5])]
        # initialize default sim_param:
        sim_param = {'proba_mutation': 1.0,
                     'proba_crossover': 1.0,
                     'global_scale_factor': 1.0,
                     'global_width_distribution': 1.0,
                     'number_best_candidates': 4,
                     'maximum': False,
                     'seed': 1.0,
                     'algorithm': 'GA',
                     'opt_steps': 2,
                     'population_size': 20,
                     'c': True, 'm': False}
        # set_param = read_input('input_file.ini', sim_param)
        parents, objf, sorted_values = get_parents(a, sim_param)
        set_param = SetParameters()
        set_param.add_parameter(Parameter('x', [-10, 10], 10,
                                          'continuous', 'uniform', 1.0, 1.0, variance=0.0,
                                          weight=1.0, active=True))
        set_param.add_parameter(Parameter('y', [-10, 10], 10,
                                          'continuous', 'uniform', 1.0, 1.0, variance=0.0,
                                          weight=1.0, active=True))
        mutated_parents = mutate(parents, set_param, sim_param, objf)
        self.assertIsInstance(mutated_parents, list)
        self.assertIsInstance(mutated_parents[0], np.ndarray)
#        self.assertEqual(len(mutated_parents),
#                        sim_param['number_best_candidates']*len(set_param.parameters))
#

    def test_crossover(self):
        seed = 10
        a = [np.array([circle(1, 2), 1, 2]),
             np.array([circle(5, 6), 5, 6]),
             np.array([circle(0, 1), 0, 1]),
             np.array([circle(0, 0), 0, 0]),
             np.array([circle(2, 0), 2, 0]),
             np.array([circle(-1, 0.5), -1, 0.5])]
        sim_param = {'proba_mutation': 1.0,
                     'proba_crossover': 1.0,
                     'global_scale_factor': 1.0,
                     'global_width_distribution': 1.0,
                     'number_best_candidates': 4,
                     'maximum': False,
                     'seed': 1.0,
                     'algorithm': 'GA',
                     'opt_steps': 2,
                     'population_size': 10,
                     'c': True, 'm': False}
        parents, objf, sorted_values = get_parents(a, sim_param)
        set_param = SetParameters()
        set_param.add_parameter(Parameter('x', [-10, 10], 10,
                                          'continuous', 'uniform', 1.0, 1.0, variance=0.0,
                                          weight=1.0, active=True))
        set_param.add_parameter(Parameter('y', [-10, 10], 10,
                                          'continuous', 'uniform', 1.0, 1.0, variance=0.0,
                                          weight=1.0, active=True))
        mutated_parents = mutate(parents, set_param, sim_param, objf)
        crossover_parents = crossover(mutated_parents, set_param, sim_param)
        number_parameters = len(set_param.parameters)
        # self.assertEqual(len(crossover_parents), sim_param['population_size'])
        seed = 10
        b = [np.array([f(1), 1]),
             np.array([f(5), 5]),
             np.array([f(0.1), 0.1]),
             np.array([f(0.2), 0.2]),
             np.array([f(2), 2]),
             np.array([f(-1), -1])]
        set_param1 = SetParameters()
        set_param1.add_parameter(Parameter('x', [-10, 10], 10,
                                           'continuous', 'uniform', 1.0, 1.0, variance=0.0,
                                           weight=1.0, active=True))
        parents, objf, sorted_values = get_parents(b, sim_param)
        mutated_parents = mutate(parents, set_param1, sim_param, objf)
        crossover_parents = crossover(mutated_parents, set_param1, sim_param)
        self.assertIsInstance(crossover_parents, list)
        self.assertIsInstance(crossover_parents[0], np.ndarray)

    def test_full_GA_one_parameter(self):
        b = [np.array([f(1), 1]),
             np.array([f(5), 5]),
             np.array([f(0.1), 0.1]),
             np.array([f(0.2), 0.2]),
             np.array([f(2), 2]),
             np.array([f(-1), -1])]
        number_candidates = 4
        parents, objf, sorted_values = get_parents(b, sim_param_full_GA)
        set_param = SetParameters()
        set_param.add_parameter(Parameter('x', [-10, 10], 10,
                                          'continuous', 'uniform', 0.1, 1.0, variance=0.0,
                                          weight=1.0, active=True))
        # mutated_parents = mutate(parents, set_param, sim_param_full_GA, objf)
        # crossover_parents = crossover(mutated_parents, set_param, sim_param_full_GA)
        number_parameters = len(set_param.parameters)
        # self.assertEqual(len(crossover_parents), sim_param_full_GA['population_size'])
        number_parameters = len(set_param.parameters)
        c = b
        sim_param_full_GA['opt_steps'] = 5
        for i in range(0, sim_param_full_GA['opt_steps']):
            b = genetic_algorithm(c, set_param, sim_param_full_GA, i)
            del c
            c = []
            for element in b:
                newpair = [float(np.asarray(f(element))), float(np.asarray(element))]
                c.append(newpair)
        self.assertTrue(c[0][0] < 0.1)
        self.assertTrue(c[0][1] < 1)

    def test_full_GA_two_params(self):
        a = [np.array([circle(1, 2), 1, 2]),
             np.array([circle(5, 5), 5, 5]),
             np.array([circle(0, 1), 0, 1]),
             np.array([circle(0, 0), 0, 0]),
             np.array([circle(2, 0), 2, 0]),
             np.array([circle(-1, 0.5), -1, 0.5])]
        parents, objf, sorted_values = get_parents(a, sim_param_full_GA)
        set_param1 = SetParameters()
        set_param1.add_parameter(Parameter('x', [-5, 5], 10,
                                           'continuous', 'uniform', 0.5, 1.0, variance=0.0,
                                           weight=1.0, active=True))
        set_param1.add_parameter(Parameter('y', [-5, 5], 10,
                                           'continuous', 'uniform', 0.5, 1.0, variance=0.0,
                                           weight=1.0, active=True))
        number_parameters = len(set_param1.parameters)
        sim_param_full_GA['opt_steps'] = 2
        c = a
        for i in range(0, sim_param_full_GA['opt_steps']):
            a = genetic_algorithm(c, set_param1, sim_param_full_GA, i)
            del c
            c = []
            for element in a:
                newpair = [float(np.asarray(circle(element[0], element[1]))),
                           float(np.asarray(element[0])),
                           float(np.asarray(element[1]))]
                c.append(newpair)
        self.assertTrue(np.isclose(c[0][0], 0.18190790013321825))

        data = [np.array([circle(1, 2), 1, 2]),
                np.array([circle(5, 5), 5, 5]),
                np.array([circle(0, 1), 0, 1]),
                np.array([circle(0, 0), 0, 0]),
                np.array([circle(2, 0), 2, 0]),
                np.array([circle(-1, 0.5), -1, 0.5])]
        sim_param_full_GA['maximum'] = True
        sim_param_full_GA['opt_steps'] = 7
        for i in range(0, sim_param_full_GA['opt_steps']):
            if i == 0:
                a = genetic_algorithm(data, set_param1, sim_param_full_GA, i)
            else:
                a = genetic_algorithm(c, set_param1, sim_param_full_GA, i)
            for element in a:
                newpair = [float(np.asarray(circle(element[0], element[1]))),
                           float(np.asarray(element[0])),
                           float(np.asarray(element[1]))]
                c.append(newpair)
        self.assertTrue(a[0][0] >= 4)

    def test_clean_constraints(self):
        seed = 10
        a = [np.array([circle(1, 2), 1, 2]),
             np.array([circle(5, 6), 5, 6]),
             np.array([circle(0, 1), 0, 1]),
             np.array([circle(0, 0), 0, 0]),
             np.array([circle(2, 0), 2, 0]),
             np.array([circle(-1, 0.5), -1, 0.5])]
        sim_param = {'proba_mutation': 1.0,
                     'proba_crossover': 1.0,
                     'global_scale_factor': 1.0,
                     'global_width_distribution': 1.0,
                     'number_best_candidates': 4,
                     'maximum': False,
                     'seed': 1.0,
                     'algorithm': 'GA',
                     'opt_steps': 2,
                     'constraints': compile('x<2', 'constraints', 'eval'),
                     'population_size': 10,
                     'c': True, 'm': False}
        parents, _, sorted_values = get_parents(a, sim_param)
        set_param = SetParameters()
        set_param.add_parameter(Parameter('x', [-10, 10], 10,
                                          'continuous', 'uniform', 1.0, 1.0, variance=0.0,
                                          weight=1.0, active=True))
        set_param.add_parameter(Parameter('y', [-10, 10], 10,
                                          'continuous', 'uniform', 1.0, 1.0, variance=0.0,
                                          weight=1.0, active=True))
        mutated_parents = mutate(parents, set_param, sim_param, fitness=[])
        crossover_parents = crossover(mutated_parents, set_param, sim_param)
        new_generation = replace_population(a, crossover_parents, sim_param['population_size'])
        self.assertEqual(len(new_generation), sim_param['population_size'])
        clean_crossover_parents = check_constraints(sim_param, set_param, crossover_parents)
        for parent in clean_crossover_parents:
            # check that every point verifies the x < 2 constraint
            assert parent[0] < 2
        number_parameters = len(set_param.parameters)


if __name__ == '__main__':
    unittest.main()
