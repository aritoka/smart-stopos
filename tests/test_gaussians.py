"""Unit tests for ga algorithm

"""
import unittest
from smartstopos.algorithms.gaussians import *
from smartstopos.utils.parameters import *



def circle(x,y):
    return x**2 +y**2

def f(x):
    return x**2

class TestGA(unittest.TestCase):
    """Unit tests for the Port class.

    """

    def test_gaussians(self):
        seed = 10
        a = [np.array([circle(1,2), 1, 2]),
             np.array([circle(5,5), 5, 5]),
             np.array([circle(0,1), 0, 1]),
             np.array([circle(0,0), 0, 0]),
             np.array([circle(2,0), 2, 0]),
             np.array([circle(-1,0.5), -1, 0,5])]
        number_candidates = 4
        set_param = SetParameters()
        set_param.add_parameter(Parameter('x', [-5,5], 10,
                                'continuous','uniform', 1.0, 1.0, variance=0.0,weight=1.0,active=False))
        set_param.add_parameter(Parameter('y', [-5,5], 10,
                                'continuous','uniform', 1.0, 1.0, variance=0.0,weight=1.0,active=False))
        sim_param = {'proba_mutation':1, 'proba_crossover':1, 'scale_factor':1,
        'number_steps' : 20, 'number_best_candidates': 4, 'maximum': False, 'seed':10,
        'width' : 1.0, 'global_width_distribution' :1.0, 'opt_steps' : 1}
        number_parameters = len(set_param.parameters.items())
        number_candidates = 4
        parents = best_candidates(a, number_candidates, False)
        self.assertEqual(len(parents), number_candidates)
        self.assertEqual(parents[0][0], 0)
        parents = best_candidates(a, number_candidates, True)
        self.assertEqual(parents[0][0], 5)
        gaussian_points = gaussians(a, set_param, sim_param, 1)
        self.assertEqual(len(gaussian_points), number_candidates*5*number_parameters)
        
        b = [np.array([f(1), 1]), 
             np.array([f(5), 5]),
             np.array([f(0.1), 0.1]),
             np.array([f(0.2), 0.2]),
             np.array([f(2), 2]), 
             np.array([f(-1), -1])]
        number_candidates = 4
        set_param1 = SetParameters()
        set_param1.add_parameter(Parameter('x', [-10,10], 10,
                                'continuous','uniform', 1.0, 1.0, variance=0.0,weight=1.0,active=False))

        number_parameters = len(set_param1.parameters.items())
        parents = best_candidates(b, number_candidates, True)
        self.assertEqual(parents[0][0], 5)
        parents = best_candidates(b, number_candidates, False)
        self.assertEqual(len(parents), number_candidates)
        self.assertEqual(parents[0][0], 0.1)
        gaussian_points = gaussians(a, set_param1, sim_param, 1)
        self.assertEqual(len(gaussian_points), number_candidates*5*number_parameters)


if __name__ == '__main__':
    unittest.main()
