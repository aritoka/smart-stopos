"""Unit tests for ga algorithm."""
import unittest
import os
import random

from smartstopos.utils.readinput import read_input


class TestReadInput(unittest.TestCase):
    """Unit tests for input reading."""

    def test_readinput1(self):
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2 m\n')
        f.write('maximum: True\n')
        f.write('proba_crossover: 0.5\n')
        f.write('proba_mutation: 0.5\n')
        f.write('global_scale_factor: 1.0\n')
        f.write('constraints:x<4\n')
        f.write('distribution:fully_random\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:1.0\n')
        f.write('type:discrete\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param = {'number_best_candidates': 10,
                     'proba_mutation': 1.0,
                     'proba_crossover': 1.0,
                     'global_scale_factor': 1.0,
                     'global_width_distribution': 1.0,
                     'maximum': False,
                     'seed': random.random(),
                     'algorithm': None,
                     'c': True, 'm': False}
        # test input
        set_param = read_input('input_file.ini', sim_param)
        self.assertEqual(len(set_param.parameters), 1)
        self.assertEqual(set_param.parameters['x'].number_points, 9)
        self.assertEqual(set_param.parameters['x'].range[0], -4)
        self.assertEqual(set_param.parameters['x'].range[1], 4)
        self.assertEqual(set_param.parameters['x'].weight, 1)
        self.assertEqual(set_param.parameters['x'].active, True)
        self.assertEqual(set_param.parameters['x'].scale_factor, 1.0)
        self.assertEqual(sim_param['run_type'], 'optimization')
        self.assertEqual(sim_param['algorithm'], 'GA')
        self.assertEqual(sim_param['distribution'], 'fully_random')
        self.assertTrue(eval(sim_param['constraints'], {'x': 1}))
        self.assertEqual(sim_param['opt_steps'], 2)
        self.assertEqual(sim_param['proba_crossover'], 0.5)
        self.assertEqual(sim_param['proba_mutation'], 0.5)
        self.assertEqual(sim_param['global_scale_factor'], 1.0)
        self.assertEqual(sim_param['m'], True)
        os.remove('input_file.ini')

    def test_readinput2(self):
        f = open('input_file.ini', 'w')
        f.write('GENERAL\n')
        f.write('run_type: optimization GA 2\n')
        f.write('proba_crossover: 0.5\n')
        f.write('proba_mutation:0.5\n')
        f.write('global_scale_factor:1.0\n')
        f.write('END_GENERAL\n\n')
        f.write('Parameter: 1\n')
        f.write('name:x\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:0.6\n')
        f.write('type:discrete\n')
        f.write('weight:0.5\n')
        f.write('end\n')
        f.write('Parameter: 2\n')
        f.write('name:y\n')
        f.write('min:-4\n')
        f.write('max:4\n')
        f.write('number_points:9\n')
        f.write('distribution:uniform\n')
        f.write('scale_factor:0.4\n')
        f.write('type:discrete\n')
        f.write('weight:0.6\n')
        f.write('end\n')
        f.write('END\n')
        f.close()
        # initialize default sim_param:
        sim_param = {'number_best_candidates': 10,
                     'proba_mutation': 1.0,
                     'proba_crossover': 1.0,
                     'global_scale_factor': 1.0,
                     'global_width_distribution': 1.0,
                     'maximum': False,
                     'seed': random.random(),
                     'algorithm': None,
                     'c': True, 'm': False}
        # test input
        set_param = read_input('input_file.ini', sim_param)
        set_param.list_parameters()
        self.assertEqual(len(set_param.parameters), 2)
        self.assertEqual(set_param.parameters['x'].number_points, 9)
        self.assertEqual(set_param.parameters['x'].weight, 0.5)
        self.assertEqual(set_param.parameters['x'].scale_factor, 0.6)
        self.assertEqual(set_param.parameters['y'].number_points, 9)
        self.assertEqual(set_param.parameters['y'].weight, 0.6)
        self.assertEqual(set_param.parameters['y'].scale_factor, 0.4)
        self.assertEqual(sim_param['run_type'], 'optimization')
        self.assertEqual(sim_param['algorithm'], 'GA')
        self.assertEqual(sim_param['opt_steps'], 2)
        self.assertEqual(sim_param['proba_crossover'], 0.5)
        self.assertEqual(sim_param['proba_mutation'], 0.5)
        self.assertEqual(sim_param['global_scale_factor'], 1.0)
        self.assertEqual(sim_param['m'], False)
        self.assertEqual(sim_param['c'], True)
        self.assertEqual(sim_param['maximum'], False)
        os.remove('input_file.ini')


if __name__ == '__main__':
    # create input files to test
    unittest.main()
