"""Unit tests for ga algorithm

"""
import unittest
from smartstopos.utils.readinput import *
from smartstopos.utils.parameters import *
from smartstopos.utils.stopostools import *
from smartstopos.pools.create_initial_pool import *
from smartstopos.pools.create_opt_pool import *


class TestParameters(unittest.TestCase):
    """Unit tests for input reading.

    """
    def test_setparam(self):
        set_param = SetParameters()
        set_param.add_parameter(Parameter('p1', [0,1], 9, 'continuous',
                                'uniform', 1.0, 1.0, variance=0.0, weight=1.0, active=True))
        self.assertEqual(len(set_param.parameters), 1)
        self.assertEqual(set_param.parameters['p1'].number_points, 9)
        self.assertEqual(set_param.parameters['p1'].range[0], 0)
        self.assertEqual(set_param.parameters['p1'].range[1], 1)
        self.assertEqual(set_param.parameters['p1'].weight, 1.0)

        set_param.add_parameter(Parameter('p2', [0.0,3.0], 5, 'discrete',
                                'uniform', 1.0, 1.0, variance=0.0, weight=0.5, active=False))
        self.assertEqual(len(set_param.parameters), 2)
        self.assertEqual(set_param.parameters['p2'].number_points, 5)
        self.assertEqual(set_param.parameters['p2'].range[0], 0)
        self.assertEqual(set_param.parameters['p2'].range[1], 3)

        set_param.add_parameter(Parameter('p3', [0.0,3.0], 5, 'continuous',
                                'uniform',1.0,1.0, variance=0.0, weight=1.0, active=True))
        self.assertEqual(len(set_param.parameters), 3)
        set_param.remove_parameter('p1')
        self.assertEqual(len(set_param.parameters), 2)
        set_param.remove_parameter('p2')
        self.assertEqual(len(set_param.parameters), 1)
        set_param.remove_parameter('p3')
        self.assertEqual(len(set_param.parameters), 0)


if __name__ == '__main__':
    unittest.main()
