import pytest
import os
import shutil

from smartstopos.duplicates.remove_duplicates import read_file, find_duplicate_lines, remove_duplicates,\
                                                     write_duplicates_file


OLD_PARAMETERS_STR = """1,0.25\n4,0.42\n6,0.9952\n2,0.778\n8,0.55984\n4,0.2684"""
NEW_PARAMETERS_STR = """4,0.25\n1,0.42\n6,0.9952\n3,0.645\n9,0.55984\n4,0.8156"""
OLD_OUTPUT = """58,1,0.25\n63,4,0.42\n97,6,0.9952\n24,2,0.778\n32,8,0.55984\n2,4,0.2684"""


@pytest.fixture
def create_opt_pool():
    os.mkdir('data')
    for filename, content in [
        ('param_set_0', OLD_PARAMETERS_STR),
        ('param_set_1', NEW_PARAMETERS_STR),
        ('csv_output.csv', OLD_OUTPUT)
    ]:
        with open('data/' + filename, 'w') as f:
            f.write(content)

    yield 'data/param_set_0', 'data/param_set_1', 'data/csv_output.csv'

    shutil.rmtree('data')


def test_read_file(create_opt_pool):
    param_set_0_file = create_opt_pool[0]

    expected_lines = OLD_PARAMETERS_STR.split('\n')
    for index, (vals, line) in enumerate(read_file(param_set_0_file, include_original_line=True)):
        expected_line = expected_lines[index].strip()
        assert line == expected_line

        expected_vals = list(map(float, line.split(',')))
        for val, exp_val in zip(vals, expected_vals):
            assert val == exp_val


def test_find_duplicate_lines(create_opt_pool):
    param_set_0_file, param_set_1_file, _ = create_opt_pool

    duplicates = find_duplicate_lines(param_set_1_file, param_set_0_file)
    expected_duplicates = {
        2: [6.0, 0.9952]
    }

    assert len(duplicates) == len(expected_duplicates)
    for index, vals in duplicates.items():
        assert index in expected_duplicates
        assert vals == expected_duplicates[index]


def test_remove_duplicates(create_opt_pool):
    param_set_0_file, param_set_1_file, _ = create_opt_pool

    shutil.copyfile(param_set_1_file, param_set_1_file + '_original')

    remove_duplicates(param_set_1_file, [2])

    duplicates = find_duplicate_lines(param_set_1_file, param_set_0_file)
    assert len(duplicates) == 0

    duplicates = find_duplicate_lines(param_set_1_file, param_set_1_file + '_original')
    assert len(duplicates) == 5


def test_write_duplicates_file(create_opt_pool):
    _, param_set_1_file, output_file = create_opt_pool

    write_duplicates_file(output_file, 'data/duplicates.csv', {2: [6.0, 0.9952]})

    lines = list(read_file('data/duplicates.csv', include_original_line=False))
    assert len(lines) == 1
    assert lines[0][0] == 97
    assert lines[0][1] == 6
    assert lines[0][2] == 0.9952


def test_main(create_opt_pool):
    param_set_0_file, param_set_1_file, output_file = create_opt_pool
    duplicate_lines = find_duplicate_lines(param_set_1_file, param_set_0_file)
    remove_duplicates(param_set_1_file, duplicate_lines)

    write_duplicates_file(output_file, 'data/duplicates.csv', duplicate_lines)

    with open(param_set_1_file) as f:
        assert len(f.readlines()) == 5

    with open('data/duplicates.csv') as f:
        assert len(f.readlines()) == 1
