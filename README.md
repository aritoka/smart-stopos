Smarstopos
----------

Description
-----------

Set of tools to run parameters explorations and perform parameters optimization locally and in HPC centers.

Installation
------------

make build

python3 setup.py build_ext --inplace



Structure 
----------
- smarstopos: 
	- algorithms:
		- ga.py: genetic algorithm
		- pso.py: TODO particle swarm optimization
		- gaussians.py: TODO using gaussians on top of best candidates

	- pools: tools to create pools of parameters for exploration and optimization 
		- create_initial_pool.py: create initial set of parameters based on input_file.ini 
		- create_opt_pool.py: create new sets of parameters based on previous results and some optimization algorithm
        

	- utils:
		- parameters.py: definition of Parameter class and SetParameters
		- readinput.py: here you can check all the input file options possible and potentially add more.
		- makedatapoints.py: based on parameter properties, creates a set of data points to explore (combination of parameter values).
		- makedatapoints_random.py: based on parameter properties, creates a set of random data points to explore.
		- stopostools.py: tools to create stopos files from an input file and from a parameters definition setting.
        - parserconstraints.py: tools to parse input algebraic expressions for constraints into actual functions

	- runscripts (For LISA and Cartesius):
	    - input_file.ini: template for input 
		- run_local.sh: bash script to run simulations locally
		- run_local_bg.sh: to submit run_local.sh as a batch job
		- run.sh: bash script to submit simualtions on HPC (assumes that STOPOS is installed)
		- stopos.job: slurm script using STOPOS as job manager (only needed in HPC)
		- analysis.job: slurm script to perform the analysis (only needed in HPC)
	- duplicates
		- remove_duplicates.py: small work around to remove already evaluated values

- tests: 
	- test_ga.py
	- test_input.py
	- test_stopostools.py
	- test_setparam.py
	- test_gaussians.py
	- test_distribution.py
	- test_constraints.py
	- test_remove_duplicates.py

- ext_tests: more complete tests
	- file: test for a workflow usign param.yaml and config.yaml
	- opt: test workflow optimization
	- single: test workflow single run

- examples: 
    - demo_wobbly_function
    - demo_wobbly_function_single_param
    - demo_wobbly_function_gaussians
    - demo_wobbly_function_constraints+
    - quartic_function
    - rastringin_function

- docs:
	- inputfile.info: information about the input keywords and options
    - sstopos.png: general overview of the code


Usage
-----
Smartstopos allows you to run a parameter exploration or parameter optimization of you python code.
In order to use it, you need to provide:
- run_program.py --> user program to be executed for different parameters 
- analysis_program.py --> user post processing program

For now, the tools assume your program is run as:

run_program.py --param1 value1 --param2 value2 ... 

Where param1 and param2 are the names of the varaibles to be varied and value1 and value2 some example values of these parameters. 

To run the simulations:

1. Create you running directory eg. MyTest

2. Create a src/ directory inside MyTest/src/

3. Copy the following files from smartstopos/runscripts/ into  MyTest/src (make sure you chose the correct cartesius vs LISA)

            - input_file.ini
            - run_local.sh (if running local)
            - run.sh (if running on HPC)
            - stopos.job (if running on HPC)
            - analysis.job (if running on HPC)
            - create_pools*.py (WIP only needed in LISA)
            - remove_duplicates.py (WIP only needed on LISA)

4. Add your run_program.py and analysis_program.py to MyTest/src

5. Modify the input_file.ini to fit your simulation (more info on the input keywords in docs/inputfile.info):

	- change the name of the run_program (+ parameters)
    - add fixed_variables if this is the case (more info on docs/input)
	- change the name of the analysis_program
    - change number of parameter and their properties
	- change paths of sstoposdir (path to this repository) and workdir (path to MyTest)
    - add a path to your venv if needed (otherwise set to NONE)
	- *change number nodes if needed
	- *change queue (only applies to stopos.job)
	- *change time limit to execute the run_program (time_run) and to do the analysis (time_analysis)
      *ONLY APPLIES FOR HPC RUNS

6. Run the script:
- 	./run_local.sh: uses smartstopos, nothing else needed (can be run interactively in HPC centers)
-   ./run_local_bg.sh: uses smartstopos, submits run_lodal.sh as a batch job to run in the background
- 	./run.sh: uses SLURM + STOPOS + smartstopos (only HPC centers)

    
Running the script will create a directory MyTest/output where the results will be dumped. For the HPC simualtions be aware that the results are initially stored in the scratch memory. You can see the path to this in the file MyTest/output/running_directory_MyTestName.
You can follow the progress of your simulations there while they are taking place. The whole directory will be copied to your working directory (MyTest/output/) as soon as the simulations are finished.

You can check whether your simulations are running by using:
squeue -u username


You can cancel all the jobs related with a run by using:
cat MyTest/ouput/jobs_id_MyTestName | xargs scancel 


Notes
-----

When performing and optimization, keep in mind that the optimization relies on the existence  of a formated output file. Make sure the final "product" of your analysis_program is a file 
in the correct format: 

    #figure_merit, #p1, #p2, #p3, ....
    figure_merit, p1, p2, p3, ....

	--> separation of columns using ','
	--> First line is for comments
	
	If your output file has a different name than csv_output.csv, 
	make sure you add the --output_file name.csv when running create_opt_pool.py 
	(modify the run_local.sh and/or analysis.job)


Contributors
------------

Ariana Torres Knoop (ariana.torres@surf.nl)
Tim Coopmans (t.coopmans@tudelft.nl)
Franciso Silva (F.HortaFerreiradaSilva@tudelft.nl)
David Maier (d.maier@tudelft.nl)

License
-------

SURFsara license?

