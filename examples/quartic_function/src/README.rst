In this example, we wish to find the optimium of the quartic function with
random noise.

.. math::

    f(x) = \sum (i x_i^4 + random(0,1)) ; x=30 dimensional, -1.28 < x_1 < 1.28

[1] J. Digalakis and K. G. Margaritus, On Benchmarking functions for genetic
algorithm, Intern. J. Computer Math., Vol. 00, pp. 1 -27, 2001

**USING THE PARAMAETERS FOR THE FILENAME IN THIS CASE IS NOT POSSIBLE, TOO LONG, LINE COMMENTED IN STOPOS.JOB***



The files in this example are as follows:

- *run_local.sh* Main script in case the example is *not* run on the supercomputer, but on a local machine. It does the following:
  * looping over several optimization runs, in each of which it determines a set of parameters to compute 'quartic function' on.
  * calls *analyse_function_output.py*, which converts the function output for each set of parameter values as stored by *quartic_function.py* into a single file *csv_output.csv* that is in the correct format to be used by the optimization script.
- *quartic_function.py*. The script which computes the quartic function. It takes the input parameter values together with a (base)name for a file <filebasename> where the script may store its output. The value of <filebasename> is automatically computed by *run_local.sh*. 
- *analyse_function_output.py*. Analyses the output running the *quartic_function.py* on all the input parameters. Collect the results in the right format for further optimization in a the file <filebasename>.csv.
- *file_parsing_tools.py*. Used by the analysis to parse the files into an *csv file
- *input_file.ini*. Configuration file which contains details on the simulation, e.g. how many parameters does 'quartic function' take (answer: 30), what is the range of values that these parameters should take, which optimization algorithm to use, etcetera.
- *run.sh* and *stopos.job* and *analysis.job*. Tools needed in case one wishes to run on the supercomputer. They perform tgether the same task as run_local by using the STOPOS software as job manager.
- *README.rst*. This documentation file.



