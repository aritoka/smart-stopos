import os
import time
import yaml
import numpy
import random
import pickle
from argparse import ArgumentParser

import netsquid as ns
from netsquid.util import DataCollector
from pydynaa.core import EventExpression

from netsquid_ae.ae_parameter_set import convert_parameter_set
from netsquid_ae.ae_classes import EndNode, RepeaterNode, HeraldedConnection, MessagingConnection, QKDNode
from netsquid_ae.ae_chain_setup import create_repeater_chain, create_qkd_application, _add_protocols_and_magic,\
    _check_sim_params

from netsquid_simulationtools.parameter_set import rootbased_improvement_fn

from netsquid_netconf.netconf import ComponentBuilder, netconf_generator

from nlblueprint.service_implementations.magic_EGP import MagicEGP
from nlblueprint.atomic_ensembles.egp_datacollector import EGPDataCollector
from nlblueprint.atomic_ensembles.ae_magic_link_layer import AEMagicLinkLayerProtocolWithSignalling

from qlink_interface import MeasurementBasis, ReqMeasureDirectly

from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder

from simulations.ae.Magic.src.forced_magic_factory import generate_forced_magic
from simulations.ae.ParameterSets.DelftParameterSet import DelftPerfectTest


def setup_ae_network(sim_params, paramfile, configfile, use_netconf=True):
    """Set up an atomic ensemble network such that it is ready to receive requests.

    Parameters
    ----------
    sim_params : dict
        Dictionary of all simulation parameters.
    use_netconf : bool (optional)
        Whether to use netconf to set up the simulation (or do it the old way).

    Returns
    -------
    network : :obj:`~netsquid.nodes.network.Network`
        Network object containing all nodes and connections.
    protocols : list of :obj:`~netsquid.protocols.protocol.Protocol`
        List of all protocols of the physical layer (entanglement generation, swapping, messaging and detection).
    magic_dist : :obj:`~netsquid_ae.ae_magic.AEMagicDistributor`
        Merged AE Magic Distributor that is used to trigger entanglement generation.

    """
    # generate quantum states for these sim params:
    if sim_params["magic"] == "forced":
        generate_forced_magic(sim_params)
        ns.sim_reset()
        # change formalism back to KET for simulation
        ns.set_qstate_formalism(ns.qubits.qubitapi.QFormalism.KET)

    # dump to yaml to be picked up by netconf
    print(paramfile)
    with open(paramfile, "w") as stream:
        yaml.dump(sim_params, stream)

    # netconf using SafeLoader breaks this part of the test but it is nevertheless nice to have it here
    # once netconf allows any kind of parameter use_netconf can be set to True
    if use_netconf:
        extra_types = [("AEEndNode", EndNode),
                       ("AERepeaterNode", RepeaterNode),
                       ("AEHeraldedConnection", HeraldedConnection),
                       ("AEMessagingConnection", MessagingConnection)]

        for extra_type in extra_types:
            ComponentBuilder.add_type(name=extra_type[0], new_type=extra_type[1])

        generator = netconf_generator(configfile)
        # The generator returns both builder-output dictionary and a dictionary with configuration
        builder_outputs, config = next(generator)
        network = builder_outputs['network']

        protocols, magic_prot, magic_dist = _add_protocols_and_magic(network, sim_params)
    else:
        protocols, network, [magic_dist, magic_prot] = create_repeater_chain(sim_params, id_str="1")
    if sim_params["encoding"] == "presence_absence":
        # for presence_absence, create second chain
        protocols2, network2, [magic_dist2, magic_prot2] = create_repeater_chain(sim_params, id_str="2")
        # combine networks
        network.combine(network2)
        protocols.extend(protocols2)
        magic_dist.merge_magic_distributor(magic_dist2)
        magic_prot.extend(magic_prot2)

    # add QKD application
    det_protos = create_qkd_application(network=network, sim_params=sim_params, measure_directly=True,
                                        initial_measurement_basis="X")
    protocols.extend(det_protos)

    # Start Protocols
    # =================
    for proto in protocols:
        proto.start()

    return network, protocols, magic_dist


def run_simulation(sim_params, use_netconf=True):
    """Setup and run a simulation for fixed set of parameters.

    Parameters
    ----------
    sim_params : dict
        Simulation parameters.
    use_netconf : bool (optional)
        Whether to use netconf to set up the simulation (or do it the old way).

    Returns
    -------
    df_meas: instance of :class:`~pandas.DataFrame`
        DataFrame containing simulation/measurement results collected with the `measurement_collector`.

    """
    # set up network
    network, protocols, magic_distributor = setup_ae_network(sim_params=sim_params, paramfile=args.paramfile, configfile=args.configfile, use_netconf=use_netconf)
    # set up EGP Service
    end_nodes = [node for node in network.nodes.values() if isinstance(node, QKDNode)]
    ae_link_layer = AEMagicLinkLayerProtocolWithSignalling(nodes=end_nodes, protocols=protocols[-2:],
                                                           magic_distributor=magic_distributor)

    ae_egp_service_alice = MagicEGP(node=end_nodes[0], magic_link_layer_protocol=ae_link_layer)
    ae_egp_service_bob = MagicEGP(node=end_nodes[1], magic_link_layer_protocol=ae_link_layer)

    ae_egp_services = [ae_egp_service_alice, ae_egp_service_bob]

    ae_link_layer.start()
    for serv in ae_egp_services:
        serv.start()

    # set up data collection
    alice_responds = EventExpression(source=ae_egp_service_alice)
    bob_responds = EventExpression(source=ae_egp_service_bob)
    both_nodes_respond = alice_responds & bob_responds

    egp_datacollector = DataCollector(EGPDataCollector(start_time=ns.sim_time()))
    egp_datacollector.collect_on(both_nodes_respond)

    for measurement_basis in [MeasurementBasis.X, MeasurementBasis.Z]:

        if measurement_basis == MeasurementBasis.X:
            y_rotation = numpy.pi / 2
        else:
            y_rotation = 0
        # randomly choose local and remote
        local = random.choice(ae_egp_services)
        remote = [service for service in ae_egp_services if service != local][0]

        # make request
        request = ReqMeasureDirectly(remote_node_id=remote.node.ID,
                                     time_unit=2,  # seconds
                                     max_time=0,
                                     number=int(sim_params["num_successes"] / 2),
                                     y_rotation_angle_local=y_rotation,
                                     y_rotation_angle_remote=y_rotation
                                     )

        # submit request
        local.measure_directly(request)
        egp_datacollector.start_time = ns.sim_time()

        # run simulation
        ns.sim_run()
        print("Request done.")

        # reset node storage
        # TODO: This becomes unnecessary with new changes in ae_protocols. but for now keep it.
        both_nodes_respond.reprime()
        for node in network.nodes.values():
            data = node.cdata
            for key in data.keys():
                data[key] = []

    return egp_datacollector.dataframe


if __name__ == "__main__":
    # parse arguments
    parser = ArgumentParser()
    parser.add_argument('--filebasename', required=False, type=str, help="Name of the file to store results in.")
    parser.add_argument("--length", required=False, type=float,
                        help="Total distance between end nodes.")
    parser.add_argument('-mlt', "--memory_lifetime", required=False, type=float,
                        help="Lifetime of the memory.")
    parser.add_argument('-mef', "--memory_efficiency", required=False, type=float,
                        help="Maximum efficiency of the memory.")
    parser.add_argument('-mpn', '--mean_photon_number', required=False, type=float,
                        help="Mean photon number of the SPDC Source.")
    parser.add_argument('-pf', '--paramfile', required=True, type=str)
    parser.add_argument('-cf', '--configfile', required=True, type=str)
    args = parser.parse_args()

    sim_params = {
        "num_repeaters": 1,             # number of repeaters in the chain (number of el. links = num_repeaters + 1 )
        "multi_photon": True,           # bool: whether to use multi-photon up to n=3
        "num_attempts": -1,             # number of clicks after which the clock stops (thus stopping the protocols)
        "num_attempts_proto": -1,       # number of clicks after which the emission protocol stops
        "num_successes": 10,         # number of successes after which the DM should stop the simulation
        "collect_states": False,        # whether to collect end-to-end states with the datacollectors
        # channel
        "length": 228.7,                # total distance between end node/ total channel length [km]
        "node_distance": -1,            # distance between nodes / length of connections [km]
        # clock
        "time_step": 5e6,               # time step for the clock in ns
        # magic
        "magic": "forced",              # type of magic ("analytical", "sampled", "forced" or None)
    }
    # import parameter set
    parameter_set = DelftPerfectTest()

    # modify parameter set
    # TODO : to_perfect_dict gives problems with modes being inf (but source requires integer)
    # parameter_set_dict = parameter_set.to_perfect_dict()
    # TODO : the parameterset is already perfect so this does not change anything
    parameter_set_dict = parameter_set.to_improved_dict(param_dict=parameter_set.to_dict(),
                                                        param_improvement_dict={"memory_T2": 0.2},
                                                        improvement_fn=rootbased_improvement_fn)
    # parameter_set_dict = parameter_set.to_dict()
    # This parameter is needed to match the magic file to be reusable
    parameter_set_dict["mean_photon_number"] = 0.01
    # manually change some things that dont require new magic to be generated
    parameter_set_dict["max_memory_efficiency"] = 0.99
    parameter_set_dict["swap_det_efficiency"] = 0.99

    # combine sim_params and parameter_set_dict into one dict
    sim_params = convert_parameter_set(sim_params, parameter_set_dict)

    # parsed arguments added to dict
    if args.length is not None:
        sim_params["length"] = args.length
        sim_params["node_distance"] = args.length / (sim_params["num_repeaters"] + 1)
        print(f"Overwriting total length to {args.memory_lifetime} as given by argument parser.")
    if args.memory_lifetime is not None:
        sim_params["memory_coherence_time"] = args.memory_lifetime
        print(f"Overwriting memory coherence time to {args.memory_lifetime} as given by argument parser.")
    if args.memory_efficiency is not None:
        sim_params["max_memory_efficiency"] = args.memory_efficiency
        print(f"Overwriting max. memory efficiency to {args.memory_efficiency} as given by argument parser.")
    if args.mean_photon_number is not None:
        sim_params["mean_photon_number"] = args.mean_photon_number
    # check full dict of simulation parameters once more after parsing
    sim_params = _check_sim_params(sim_params)

    print(sim_params)

    # run simulation (setup, forced magic then actual QKD experiment)
    measurement_df = run_simulation(sim_params, use_netconf=True)

    meas_holder = RepchainDataFrameHolder(data=measurement_df, number_of_nodes=2,
                                          baseline_parameters=sim_params)

    # save as csv (and make sure nothing is overwritten)
    if not os.path.exists("raw_data"):
        os.mkdir("raw_data")

    saved = False

    while not saved:
        timestr = time.strftime("%Y%m%d-%H%M%S")
        # cast parsed arguments to string
        argstring = str(vars(args)).translate({**{ord(i): None for i in "{' }"}, **{ord(j): "_" for j in ":,"}})
        filename = "raw_data/" + argstring + "_" + timestr

        if not os.path.isfile(filename):
            with open(filename + "_measurement.pickle", 'wb') as handle:
                pickle.dump(meas_holder, handle, protocol=pickle.HIGHEST_PROTOCOL)
            saved = True
