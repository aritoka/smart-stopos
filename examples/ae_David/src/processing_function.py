from netsquid_simulationtools.repchain_data_process import fast_process_qkd_data


if __name__ == "__main__":
    # TODO: add argument parser to enable passing varied params?
    fast_process_qkd_data(raw_data_dir="raw_data", suffix="_measurement.pickle", varied_params=["length"],
                          use_multiprocessing=True,
                          csv_output_files=("output.csv", "csv_output.csv"), plot_processed_data=False)
