# Simulation script for an atomic ensembles repeater chain (with n repeaters)

import logging
from argparse import ArgumentParser
import os
import time
import pickle
import pandas
import numpy

import netsquid as ns
from netsquid.util import DataCollector
from netsquid.util.simtools import logger
from pydynaa.core import EventExpression, ExpressionHandler

from netsquid_ae.ae_chain_setup import create_repeater_chain, create_qkd_application, _check_sim_params
from netsquid_ae.ae_protocols import ExtractionProtocol
from netsquid_ae.protocol_event_types import EVTYPE_SUCCESS
from netsquid_ae.datacollectors.qkd_decisionmaker import DecisionMaker
from netsquid_ae.datacollectors.param_scan_runner import do_parameter_scan
from netsquid_ae.datacollectors.measurement_collector import MeasurementCollector
from netsquid_ae.datacollectors.chain_collector import ChainStateCollector
from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder
from netsquid_simulationtools.repchain_data_process import process_container_qkd
from netsquid_ae.ae_parameter_set import convert_parameter_set
from netsquid_ae.ae_classes import QKDNode

from simulations.ae.ParameterSets.GenevaParameterSet import Geneva2020Measured, Geneva2020Expected, Geneva2023Projected
from simulations.ae.ParameterSets.DelftParameterSet import Delft2020Measured, Delft2020Expected, Delft2023Projected
from simulations.ae.ParameterSets.ICFOParameterSet import ICFO2020Measured2LevelAFC, ICFO2020Expected2LevelAFC, \
    ICFO2023Projected2LevelAFC
from simulations.ae.ParameterSets.ParisParameterSet import Paris2020Measured, Paris2020Expected, Paris2023Projected

from simulations.ae.Magic.src.forced_magic_factory import generate_forced_magic

# from netsquid_ae.parameter_set_collection import Guha2015p013


def run_simulation(sim_params):
    """Setup and run a simulation for fixed set of parameters.

    Parameters
    ----------
    sim_params : dict
        Simulation parameters.

    Returns
    -------
    df_meas: instance of :class:`~pandas.DataFrame`
        DataFrame containing simulation/measurement results collected with the `measurement_collector`.
    df_state: instance of :class:`~pandas.DataFrame`
        DataFrame containing end-to-end state and relevant data collected with the `chain_collector`.

    """
    ns.sim_reset()

    # generate quantum states for these sim params:
    if sim_params["magic"] == "forced":
        generate_forced_magic(sim_params)
        ns.sim_reset()
        # change formalism back to KET for simulation
        ns.set_qstate_formalism(ns.qubits.qubitapi.QFormalism.KET)

    protocols, network, [magic_dist, magic_prot] = create_repeater_chain(sim_params)
    if sim_params["encoding"] == "presence_absence":
        # for presence_absence, create second chain
        protocols2, network2, [magic_dist2, magic_prot2] = create_repeater_chain(sim_params, id_str="2")
        # combine networks
        network.combine(network2)
        protocols.extend(protocols2)
        magic_dist.merge_magic_distributor(magic_dist2)
        magic_prot.extend(magic_prot2)

    nodes = list(network.nodes.values())
    # add QKD application
    det_protos = create_qkd_application(network=network, sim_params=sim_params, measure_directly=True,
                                        initial_measurement_basis="X")
    protocols.extend(det_protos)

    # Start Protocols
    # =================
    for proto in protocols:
        proto.start()
    # Start Clocks
    # ================
    # for magic clocks dont matter, the MagicDistributor triggers the protocols by delivering a message
    if not sim_params["magic"]:
        for node in nodes:
            if not isinstance(node, QKDNode):
                # Note: QKD nodes do not have clocks
                node.subcomponents["Clock"].start()
    else:
        # start auto delivery on merged MagicDistributor
        magic_dist.start_auto_delivery()

    # Setup data collection
    # =====================
    # setting up EventExpressions
    evexpr_succ_a = EventExpression(event_type=EVTYPE_SUCCESS, source=protocols[-2])
    evexpr_succ_b = EventExpression(event_type=EVTYPE_SUCCESS, source=protocols[-1])
    evexpr_success = evexpr_succ_a and evexpr_succ_b

    # Measurements collected when end node receive full message containing all measurement results
    measurement_collector = DataCollector(MeasurementCollector(messaging_protocol_alice=protocols[-2],
                                                               messaging_protocol_bob=protocols[-1]))
    measurement_collector.collect_on(evexpr_success)

    # States collected by from Extraction Protocols while looking up all BSM outcomes directly
    if sim_params["collect_states"]:
        if sim_params["encoding"] == "time_bin":
            chain_state_collector = DataCollector(ChainStateCollector(nodes_1=nodes))
        else:
            chain_state_collector = DataCollector(ChainStateCollector(nodes_1=nodes[:len(nodes) // 2],
                                                                      nodes_2=nodes[len(nodes) // 2:]))

        ex_protos = [protocol for protocol in protocols if protocol.protocol_type == "Extraction"]
        if sim_params["encoding"] == "time_bin":
            chain_state_collector.collect_on([(ex_protos[0], ExtractionProtocol.evtype_extract),
                                              (ex_protos[1], ExtractionProtocol.evtype_extract)], "AND")
        else:
            chain_state_collector.collect_on([(ex_protos[0], ExtractionProtocol.evtype_extract),
                                              (ex_protos[1], ExtractionProtocol.evtype_extract),
                                              (ex_protos[2], ExtractionProtocol.evtype_extract),
                                              (ex_protos[3], ExtractionProtocol.evtype_extract)], "AND")

    # Decisionmaker stopping simulation and changing measurement basis waiting on messaging protocols
    dm = DecisionMaker(min_successes=sim_params["num_successes"], datacollector=measurement_collector,
                       alice_proto=protocols[-2], bob_proto=protocols[-1])
    decision_handler = ExpressionHandler(dm.decide)
    dm._wait(decision_handler, expression=evexpr_success)

    # Run simulation
    # ==============
    print("starting simulation with sim_parms", sim_params)
    ns.sim_run()

    if sim_params["collect_states"]:
        # filter chain_state_collector for non-empty lines
        df = chain_state_collector.dataframe
        df = df.fillna(value="NAN")
        df = df.where(pandas.notnull(df), None)
        chain_data = df[df.state != "NAN"]
    else:
        chain_data = pandas.DataFrame()

    return measurement_collector.dataframe, chain_data


def test_sets():
    """Small function to test all ParameterSets. Unused, here to pass lint."""
    for pset in [Delft2020Measured, Delft2020Expected, Delft2023Projected, Geneva2020Measured, Geneva2020Expected,
                 Geneva2023Projected, ICFO2020Measured2LevelAFC, ICFO2020Expected2LevelAFC, ICFO2023Projected2LevelAFC,
                 Paris2020Measured, Paris2020Expected, Paris2023Projected]:
        pset()


if __name__ == "__main__":
    # parse arguments
    parser = ArgumentParser()
    parser.add_argument('--filebasename', required=False, type=str, help="Name of the file to store results in.")
    parser.add_argument('-mpn', '--mean_photon_number', required=False, type=float,
                        help="Mean photon number of the SPDC Source.")
    parser.add_argument('-tm', '--total_num_modes', required=False, type=int,
                        help="Number of total modes of the SPDC Source.")
    parser.add_argument("--num_successes", required=False, type=int,
                        help='Number of entangled pairs to distribute between end nodes.')
    parser.add_argument("--length", required=False, type=float,
                        help="Total distance between end nodes.")
    parser.add_argument('-mlt', "--memory_lifetime", required=False, type=float,
                        help="Lifetime of the memory.")
    parser.add_argument('-mef', "--memory_efficiency", required=False, type=float,
                        help="Maximum efficiency of the memory.")
    args = parser.parse_args()

    sim_params = {
        "num_repeaters": 1,             # number of repeaters in the chain (number of el. links = num_repeaters + 1 )
        "multi_photon": True,           # bool: whether to use multi-photon up to n=3
        "num_attempts": -1,             # number of clicks after which the clock stops (thus stopping the protocols)
        "num_attempts_proto": -1,       # number of clicks after which the emission protocol stops
        "num_successes": 1000,            # number of successes after which the DM should stop the simulation
        "collect_states": False,        # whether to collect end-to-end states with the datacollectors
        # channel
        "length": 20,                  # total distance between end node/ total channel length [km]
        "node_distance": -1,            # distance between nodes / length of connections [km]
        # clock
        "time_step": 5e6,               # time step for the clock in ns
        # magic
        "magic": "forced",              # type of magic ("analytical", "sampled", "forced" or None)
    }

    # Specify ParameterSet:
    # parameter_set = Guha2015p013()
    # memory_type = None
    # Memory comparison
    total_num_modes_mem_comp = 1000
    memory_type = "AFC"
    # memory_type = "EIT"
    # Delft
    # parameter_set = Delft2020Measured()
    # parameter_set = Delft2020Expected()
    parameter_set = Delft2023Projected()

    # Geneva
    # parameter_set = Geneva2020Measured()
    # parameter_set = Geneva2020Expected()
    # parameter_set = Geneva2023Projected()

    # Barcelona
    # parameter_set = ICFO2020Measured2LevelAFC()
    # parameter_set = ICFO2020Expected2LevelAFC()
    # parameter_set = ICFO2023Projected2LevelAFC()

    # Paris
    # parameter_set = Paris2020Measured()
    # parameter_set = Paris2020Expected()
    # parameter_set = Paris2023Projected()

    # overwrite sim_params with parsed arguments
    if args.num_successes is not None:
        sim_params["num_successes"] = args.num_successes
    if args.length is not None:
        sim_params["length"] = args.length

    # combine sim_params and parameter set into one dict
    sim_params = convert_parameter_set(sim_params, parameter_set)

    # overwrite parameters from param_set with parsed arguments
    if args.total_num_modes is not None:
        sim_params["spectral_modes"] = args.total_num_modes
        sim_params["temporal_modes"] = 1
        sim_params["spatial_modes"] = 1
        print(f"Overwriting total number of modes to {args.total_num_modes} as given by argument parser.")
    if args.mean_photon_number is not None:
        sim_params["mean_photon_number"] = args.mean_photon_number
        print(f"Overwriting mean photon number to {args.mean_photon_number} as given by argument parser.")
    if args.memory_lifetime is not None:
        sim_params["memory_coherence_time"] = args.memory_lifetime
        print(f"Overwriting memory coherence time to {args.memory_lifetime} as given by argument parser.")
    if args.memory_efficiency is not None:
        sim_params["max_memory_efficiency"] = args.memory_efficiency
        print(f"Overwriting max. memory efficiency to {args.memory_efficiency} as given by argument parser.")

    # Memory comparison
    if memory_type == "AFC":
        # AFC memory
        sim_params["memory_time_dependence"] = "exponential"
        sim_params["spectral_modes"] = total_num_modes_mem_comp
        sim_params["temporal_modes"] = 1
        sim_params["spatial_modes"] = 1
        print(f"AFC Memory - setting: total number of modes="
              f"{(sim_params['spectral_modes'] * sim_params['temporal_modes'] * sim_params['spatial_modes'])},"
              f" maximum efficiency={sim_params['max_memory_efficiency']} with {sim_params['memory_time_dependence']}"
              f" decay.")
    elif memory_type == "EIT":
        # EIT memory
        sim_params["memory_time_dependence"] = "gaussian"
        sim_params["spectral_modes"] = total_num_modes_mem_comp
        sim_params["temporal_modes"] = 1
        sim_params["spatial_modes"] = 1
        print(f"EIT Memory - setting: total number of modes="
              f"{(sim_params['spectral_modes'] * sim_params['temporal_modes'] * sim_params['spatial_modes'])},"
              f" maximum efficiency={sim_params['max_memory_efficiency']} with {sim_params['memory_time_dependence']}"
              f" decay.")
    elif memory_type is None:
        print(f"No memory specified using parameters from parameter_set - total number of modes="
              f"{(sim_params['spectral_modes'] * sim_params['temporal_modes'] * sim_params['spatial_modes'])},"
              f" maximum efficiency={sim_params['max_memory_efficiency']} with {sim_params['memory_time_dependence']}"
              f" decay.")
    else:
        raise ValueError(f"Unknown memory type: {memory_type}. Please choose 'AFC' or 'EIT'.")

    sim_params['sample_file'] = 'guha_p013_td_v9_74km_10k.pickle' + '_' + str(int(sim_params['length']))
    # check all sim_params once again after parsing
    sim_params = _check_sim_params(sim_params)

    run_single_simulation = True
    store_data = True

    start_time = time.time()

    # set debugging or info
    # logger.setLevel(logging.INFO)
    logger.setLevel(logging.DEBUG)

    if run_single_simulation:
        # Run simulation for fixed parameters:
        df_meas, df_state = run_simulation(sim_params)
    else:
        # Run parameter scan:
        scan_param_name = "length"
        if sim_params["magic"] == "sampled":
            # Scan over all available sampled elementary link data
            states_container = pickle.load(open(sim_params["sample_file"], "rb"))
            el_node_dists = sorted(states_container.dataframe.node_distance.unique().tolist())
            scan_param_range = [i * (sim_params["num_repeaters"] + 1) for i in el_node_dists]
        else:
            max_value = 100
            min_value = 1
            num_steps = 30
            step_size = (max_value - min_value) / num_steps
            if isinstance(sim_params[scan_param_name], int):
                step_size = int(step_size)
                scan_param_range = list(range(min_value, max_value + step_size, step_size))
            else:
                scan_param_range = list(numpy.linspace(min_value, max_value + step_size, num_steps))

        df_meas, df_state = do_parameter_scan(sim_params, [scan_param_name], [scan_param_range], run_simulation)

    # wrap dataframes in RepchainDataframeHolder object
    num_nodes = sim_params["num_repeaters"] + 2

    meas_holder = RepchainDataFrameHolder(data=df_meas, number_of_nodes=num_nodes,
                                          baseline_parameters=sim_params, description="Dataframe containing end node"
                                                                                      "measurement results")
    if sim_params["collect_states"]:
        state_holder = RepchainDataFrameHolder(data=df_state, number_of_nodes=num_nodes,
                                               baseline_parameters=sim_params, description="Dataframe containing"
                                                                                           "end node"
                                                                                           "measurement results")
    if not run_single_simulation:
        meas_holder.varied_parameters = [scan_param_name]
        df_sk = process_container_qkd(meas_holder)
        # plot secret key rate
        # plot_scripts.skr_qber_attempts(scan_param_name, df_sk, meas_holder.baseline_parameters)

    print("############################  RESULTS ###################################")

    runtime = time.time() - start_time

    '''parameter_string = ""
    for param_key in sim_params:
        df[param_key] = sim_params[param_key]
        parameter_string += "{}={},".format(param_key, sim_params[param_key])
    parameter_string = parameter_string[:-1]
    # parameter_string = "param"

    if parameter_string != "":
        parameter_string = " at " + parameter_string'''

    print("finished AE Rep Chain simulation, runtime {} seconds for {} successes.\n"
          .format(runtime, sim_params["num_successes"]))

    if store_data:
        # save as csv (and make sure nothing is overwritten)
        if not os.path.exists("raw_data"):
            os.mkdir("raw_data")

        saved = False

        while not saved:
            timestr = time.strftime("%Y%m%d-%H%M%S")
            # cast parsed arguments to string
            argstring = str(vars(args)).translate({**{ord(i): None for i in "{' }"}, **{ord(j): "_" for j in ":,"}})
            filename = "raw_data/" + argstring + "_" + timestr

            if not os.path.isfile(filename):
                with open(filename + "_measurement.pickle", 'wb') as handle:
                    pickle.dump(meas_holder, handle, protocol=pickle.HIGHEST_PROTOCOL)
                if sim_params["collect_states"]:
                    with open(filename + "_states.pickle", 'wb') as handle:
                        pickle.dump(state_holder, handle, protocol=pickle.HIGHEST_PROTOCOL)
                if not run_single_simulation:
                    df_sk.to_csv(filename + "_qkd_data.csv", index=False)

                saved = True
