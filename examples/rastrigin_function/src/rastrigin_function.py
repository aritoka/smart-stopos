"""
Usage:
    python3 function.py -filebasename <folder-to-store-output>/<somefilebasename> -x1 0.2 -x2 -0.5 (...) -x20 0.1
"""

from argparse import ArgumentParser
import csv
import numpy as np

def function(x):
    '''Returns Rastrigin's function value'''
    
    input_array = np.array(x)
    rastrigin = 200 + np.sum(input_array**2) - 10 * np.sum(np.cos(2*np.pi*input_array))

    return rastrigin


if __name__ == "__main__":
    # Parse the input argument
    parser = ArgumentParser()
    parser.add_argument('--filebasename', type=str)
    parser.add_argument('--x1', type=float)
    parser.add_argument('--x2', type=float)
    parser.add_argument('--x3', type=float)
    parser.add_argument('--x4', type=float)
    parser.add_argument('--x5', type=float)
    parser.add_argument('--x6', type=float)
    parser.add_argument('--x7', type=float)
    parser.add_argument('--x8', type=float)
    parser.add_argument('--x9', type=float)
    parser.add_argument('--x10', type=float)
    parser.add_argument('--x11', type=float)
    parser.add_argument('--x12', type=float)
    parser.add_argument('--x13', type=float)
    parser.add_argument('--x14', type=float)
    parser.add_argument('--x15', type=float)
    parser.add_argument('--x16', type=float)
    parser.add_argument('--x17', type=float)
    parser.add_argument('--x18', type=float)
    parser.add_argument('--x19', type=float)
    parser.add_argument('--x20', type=float)
    
    args = parser.parse_args()
    param_dict = vars(args)    
    parameter_values = []
    for i in range (20):
        param_name = 'x' + str(i+1)
        parameter_values.append(param_dict[param_name])
    # Run the "simulation"
    output_value = function(x=parameter_values)

    # Store the output in a file
    csv_filename = args.filebasename + ".csv"
    with open(csv_filename, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow([output_value] + parameter_values)
