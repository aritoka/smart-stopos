"""
Usage:
    python3 function.py -filebasename <folder-to-store-output>/<somefilebasename> -x 3
"""

from argparse import ArgumentParser
import csv
import numpy as np

def function(x):
    '''Returns wobbly function value'''
    radius_squared = (x) ** 2 
    return radius_squared + np.sin(radius_squared)


if __name__ == "__main__":
    # Parse the input argument
    parser = ArgumentParser()
    parser.add_argument('--filebasename', type=str)
    parser.add_argument('--x', type=float)
    args = parser.parse_args()
    parameter_values = [args.x]

    # Run the "simulation"
    output_value = function(x=args.x)
    
    # Store the output in a file
    csv_filename = args.filebasename + ".csv"
    with open(csv_filename, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow([output_value] + parameter_values)
