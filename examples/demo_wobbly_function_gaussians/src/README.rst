In this example, we wish to find the optimium of the function

.. math::

    f(x, y) = x^2 + y^2 + \sin(x^2 + y^2) 

which we have dubbed 'wobbly function'.

The files in this example are as follows:

- *run_local.sh* Main script in case the example is *not* run on the supercomputer, but on a local machine. It does the following:
    1. Creates a list fo parameters to explore using smart-stopos
    2. Runs *wobbly_function.py* with each ot these parameters
    3. Calls*analyse_function_output.py*, which converts *wobbly_function.py* output for each set of parameter vinto a single file *csv_output.csv* in the correct format to be used by the optimization scripts of smartstopos
    4. Creates a new set of parameters to explored based on the optimization algorithm using smart-stopos.
    5. Goes back to step 2) until all optimization steps have been performed.

- *wobbly_function.py*. The script which computes the wobbly function. It takes the input parameter values together with a (base)name for a file <filebasename> where the script may store its output as input. The value of <filebasename> is automatically computed by *run_local.sh* and is related to the input parameters. 

- *analyse_function_output.py*. See *run_local.sh*.

- *file_parsing_tools.py*. Parsing tools used by *analyse_function_output.py*.

- *input_file.ini*. Configuration file which contains details on the simulation, e.g. how many parameters does 'wobbly function' take (answer: 2), what is the range of values that these parameters should take, which optimization algorithm to use, et cetera.

- *run.sh* , *stopos.job* and *analysis.job*. Tools needed in case one wishes to run on the supercomputer.

- *README.rst*. This documentation file.

